# NCSU Menu Notifier

A website that will notify you when your favorite foods are being served in the NC State dining halls. Try it out at <https://www.ncsu-mn.com/>!

## How it works

Uses a web scraper to download the menu every day. That data is then parsed and saved in a database. Every day, the site checks which dishes are being served for that meal and updates personalized RSS feeds for users who picked those dishes.

Prior to v3, this used to be a Django site which sent notifications via text messages. The old code can be found in the `v2` folder. See [CHANGELOG.md](./CHANGELOG.md) for more historical context.

## Development Environment Setup

```bash
# Install Deno
curl -fsSL https://deno.land/install.sh | sh
# Install deployctl
deno install -A jsr:@deno/deployctl --global
# Set up pre-commit hook
cat > .git/hooks/pre-commit << EOF
#!/usr/bin/env bash
set -Eeuo pipefail
deno fmt --check
deno task test
EOF
chmod +x .git/hooks/pre-commit
```

* To run the site locally: `deno task dev`
* To run the scraper (requires editing `menu-downloader.ts` first): `deno task download`
* To regenerate dishes.json: `deno task regen`
* To test: `deno task test`
* To preview a deployment: `deno task preview` (note: still uses the real database)
* To deploy: `deno task deploy`

## Design

This app consists of a Deno Deploy project which has a few routes and a [cron task](https://docs.deno.com/deploy/kv/manual/cron/) for scraping the menu.

* `/`: Home page which describes what the site does.
* `/signup/`: Lists all dishes, allowing users to subscribe to their favorite foods. The form `POST`s to `/signup/` , which saves the list of foods and returns a unique RSS feed URL.
* `/rss/:uid/atom.xml`: The feed URL for a particular user, which updates when any of their favorite foods are being served that day. Supports cache headers and conditional requests since the content only changes once per day.

## Data Model

Data is stored in [Deno KV](https://docs.deno.com/deploy/kv/manual/) in a few different keyspaces which are described below.

### Globals

* `["globals", "lastUpdated"] = new Date()`: Used to set the Last-Modified header and the updated date when serving RSS feeds. Gets updated every day after the menu is downloaded.
* `["globals", "nextUID"] = bigint`: An auto-incrementing ID used for generating unique feed URLs.

### UIDs

Associates user IDs with a list of `KvKeys` that can be used to look up the serving status of the user's favorite foods.

* `["uids", bigint] = [["dishStatuses", dishName1], ["dishStatuses", dishName2], ...]`
    - Example: `["uids", 1414] = [["dishStatuses", "vegan bulgogi"], ["dishStatuses", "baked apples"]]`

### Dish Statuses

Associates dish names with the list of places and meals that dish is being served. These keys expire after 22 hours to keep the data fresh.

* `["dishStatuses", dishName] = [DishStatus1, DishStatus2, ...]`
    - Example: `["dishStatuses", "vegan bulgogi"] = [{meal: "lunch", diningHall: "Fountain"}, {meal: "fastlunch", diningHall: "University Towers"}]`

### Dish Logs

Ensures we have a record of all dishes, the last time they were served, and various traits (vegan, allergens, etc.). Mainly used for regenerating the list of dishes for the signup page.

* `["dishLogs", dishName] = {lastServed: new Date(), traits: string[]}`

## How dishes were imported

```sql
SELECT DISTINCT LOWER(name) FROM ncsumn_menuitem INNER JOIN ncsumn_menuitemlog ON ncsumn_menuitemlog.menu_item_id=ncsumn_menuitem.id WHERE ncsumn_menuitemlog.date_served > '2024-01-01'";
```

```sql
SELECT name, count(*) AS ct FROM ncsumn_menuitem INNER JOIN ncsumn_notifieruser_fav_foods ON ncsumn_menuitem.id=menuitem_id GROUP BY name ORDER BY ct DESC LIMIT 100;
```

## Other supporting infrastructure

* This GitLab repo, and GitLab CI (with auth tokens for Deno Deploy)
* A Deno Deploy project, with a custom domain configured
* Synthetic monitoring via Grafana Cloud
* Domain registration, DNS records, and CDN configured in Cloudflare
* A Google Cloud Storage bucket for storing Deno KV backups
* A Google Cloud [Service Account](https://docs.deno.com/deploy/kv/manual/backup/#configuring-backup-to-google-cloud-storage) for accessing the bucket.
* A Cloudflare [Bulk Redirect Rule](https://developers.cloudflare.com/rules/url-forwarding/bulk-redirects/) to redirect requests from the apex to www.
* A Cloudflare [Cache Rule](https://til.simonwillison.net/cloudflare/cloudflare-cache-html) to allow caching of HTML files.
* Cloudflare [Email Routing](https://developers.cloudflare.com/email-routing/) for bugs@ncsu-mn.com
* Other non-default Cloudflare settings:
  - Always Use HTTPS: On
  - HTTP Strict Transport Security (HSTS): On
  - Minimum TLS Version: TLS 1.2
  - Security Level: Essentially Off ([Stop deploying web application firewalls](https://www.macchaffee.com/blog/2023/wafs/))
  - Browser Integrity Check: Off
  - Replace insecure JavaScript libraries: Off
  - Tiered Cache Topology: Smart Tiered Cache Topology
  - IP Geolocation: Off
  - Network Error Logging: Off
  - Rate limit: 30 requests per IP per 10 second interval
