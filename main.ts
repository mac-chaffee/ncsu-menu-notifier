import { serveDir } from "jsr:@std/http/file-server";
import { rssHandler } from "./src/rss.ts";
import { signupHandler } from "./src/signup.ts";
import { downloadHandler } from "./src/menu-downloader.ts";

// 3AM EST == 08:00 UTC
// 4AM EDT == 08:00 UTC
Deno.cron("Download the menu", "0 8 * * *", downloadHandler);

Deno.serve(async (req: Request) => {
  const pathname = new URL(req.url).pathname;
  const kv = await Deno.openKv();
  if (pathname.startsWith("/rss")) {
    return await rssHandler(req, kv);
  }
  if (pathname.startsWith("/signup") && req.method === "POST") {
    return await signupHandler(req, kv);
  }
  return serveDir(req, {
    fsRoot: "./public",
    headers: [
      "cache-control: public; max-age=3600",
    ],
  });
});
