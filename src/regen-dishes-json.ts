// Execute this script with "deno task regen" to regenerate public/signup/dishes.json
// based on dishLogs stored in DenoKV.
// Set DENO_KV_ACCESS_TOKEN
import { listDishLogs, NamedDishLog } from "./storage.ts";
import { BLOCKLIST } from "./blocklist.ts";
import { POPULAR } from "./popular.ts";
import currentDishesJson from "../public/signup/dishes.json" with {
  type: "json",
};

// This is the format for dishes that tom-select requires
interface DishOption {
  text: string;
  optgroup: string;
  traits: string[];
}

// This is a map used to avoid O(n^2) looping
interface DishOptionMap {
  [name: string]: DishOption;
}

// Convert current dishes into a map
const currentDishes = currentDishesJson as DishOption[];
const dishMap: DishOptionMap = {};
for (const dish of currentDishes) {
  Object.assign(dishMap, { [dish.text]: dish });
}

const URL = Deno.env.get("DENO_KV_PROD_URL");
const TOKEN = Deno.env.get("DENO_KV_ACCESS_TOKEN");

// Comment this out if debugging locally
if (URL === undefined || TOKEN === undefined) {
  throw new Error(
    "You must add DENO_KV_PROD_URL and DENO_KV_ACCESS_TOKEN to .env-BfK2CeRx",
  );
}

const kv = await Deno.openKv(Deno.env.get("DENO_KV_PROD_URL"));

const entries = listDishLogs(kv);
for await (const entry of entries) {
  const log: NamedDishLog = { name: entry.key[1] as string, log: entry.value };

  if (Object.hasOwn(dishMap, log.name)) {
    // Augment existing entries with the latest traits from the DB
    dishMap[log.name].traits = log.log.traits;
  } else {
    // Create any new entries
    const optgroup = POPULAR.has(log.name) ? "popular" : "all";
    dishMap[log.name] = {
      text: log.name,
      optgroup: optgroup,
      traits: log.log.traits,
    };
  }
}

// Write out the updated dishes.json
const result: DishOption[] = [];

for (const [_, option] of Object.entries(dishMap)) {
  // Ignore any newly-blocked dishes
  if (BLOCKLIST.has(option.text)) {
    continue;
  }
  // TODO: Can set optgroups here
  result.push(option);
}

// Sort by dish name for display consistency
result.sort((a, b) => a.text.localeCompare(b.text));

// Add spacing for better diffs. When gzipped, size difference is negligible.
await Deno.writeTextFile(
  "./public/signup/dishes.json",
  JSON.stringify(result, null, 2),
);
