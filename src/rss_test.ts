import { formatMessage } from "./rss.ts";
import { DiningHall, Meal, type NamedDishStatus } from "./storage.ts";
import { assertEquals } from "jsr:@std/assert";

Deno.test("formatMessage - single status", () => {
  const input: NamedDishStatus = {
    name: "vegan bulgogi",
    statuses: [{
      meal: Meal.Lunch,
      diningHall: DiningHall.Clark,
    }],
  };

  const result = formatMessage(input);
  assertEquals(
    result,
    "They're serving vegan bulgogi at Clark for lunch today!",
  );
});

Deno.test("formatMessage - multiple statuses, same location different meals", () => {
  const input: NamedDishStatus = {
    name: "hashbrowns",
    statuses: [
      {
        meal: Meal.Breakfast,
        diningHall: DiningHall.Fountain,
      },
      {
        meal: Meal.Lunch,
        diningHall: DiningHall.Fountain,
      },
    ],
  };

  const result = formatMessage(input);
  assertEquals(
    result,
    "They're serving hashbrowns at Fountain for breakfast and lunch today!",
  );
});

Deno.test("formatMessage - multiple locations, multiple meals", () => {
  const input: NamedDishStatus = {
    name: "hashbrowns",
    statuses: [
      {
        meal: Meal.Breakfast,
        diningHall: DiningHall.Fountain,
      },
      {
        meal: Meal.Breakfast,
        diningHall: DiningHall.Case,
      },
      {
        meal: Meal.Breakfast,
        diningHall: DiningHall.Clark,
      },
      {
        meal: Meal.Fastlunch,
        diningHall: DiningHall.UniversityTowers,
      },
      {
        meal: Meal.Lunch,
        diningHall: DiningHall.Fountain,
      },
      {
        meal: Meal.Dinner,
        diningHall: DiningHall.Fountain,
      },
    ],
  };

  const result = formatMessage(input);
  assertEquals(
    result,
    "They're serving hashbrowns at the following locations today:\n<ul>\n<li>Fountain for breakfast, lunch, and dinner</li>\n<li>Case for breakfast</li>\n<li>Clark for breakfast</li>\n<li>University Towers for fastlunch</li>\n</ul>",
  );
});
