import {
  DiningHall,
  getDishStatuses,
  getLastUpdated,
  Meal,
  NamedDishStatus,
} from "./storage.ts";

import { getBaseURL } from "./signup.ts";

function formatList(items: string[]): string {
  if (items.length === 1) return items[0];
  if (items.length === 2) return `${items[0]} and ${items[1]}`;
  return `${items.slice(0, -1).join(", ")}, and ${items[items.length - 1]}`;
}

// Generates the text of the RSS feed for a given dish and list of locations/meals.
// Dishes served at only one location result in a single sentence.
// Multiple locations are put into a <ul>.
export function formatMessage(namedStatus: NamedDishStatus): string {
  const { name, statuses } = namedStatus;

  // Group by dining hall
  const byHall = statuses.reduce((acc, status) => {
    if (!acc[status.diningHall]) {
      acc[status.diningHall] = [];
    }
    acc[status.diningHall].push(status.meal);
    return acc;
  }, {} as Record<DiningHall, Meal[]>);

  // If only one dining hall, we can return a simple sentence
  if (Object.keys(byHall).length === 1) {
    const [hall1, meals] = Object.entries(byHall)[0];
    return `They're serving ${name} at ${hall1} for ${
      formatList(meals)
    } today!`;
  }
  // Otherwise, generate a bulleted list for clarity
  let ret = `They're serving ${name} at the following locations today:\n<ul>\n`;
  Object.entries(byHall).map(([hall, meals]) => {
    ret += `<li>${hall} for ${formatList(meals)}</li>\n`;
  });
  return `${ret}</ul>`;
}

// Copied from Tera:
// https://github.com/Keats/tera/blob/ae13d7ce39d732aae3f68435ed52c60732fe0ee0/src/builtins/filters/string.rs#L336
// This is needed because the Atom format requires the content to be escaped.
// https://www.rfc-editor.org/rfc/rfc4287#section-4.1.3.3
function escapeXML(value: string): string {
  const result: string[] = new Array(value.length * 2);
  let index = 0;
  for (const char of value) {
    switch (char) {
      case "&":
        result[index++] = "&amp;";
        break;
      case "<":
        result[index++] = "&lt;";
        break;
      case ">":
        result[index++] = "&gt;";
        break;
      case '"':
        result[index++] = "&quot;";
        break;
      case "'":
        result[index++] = "&apos;";
        break;
      default:
        result[index++] = char;
    }
  }
  return result.join("");
}

async function generateFeed(
  kv: Deno.Kv,
  uid: string,
  lastUpdated: Date,
  baseURL: string,
): Promise<Response> {
  let uidInt = 0n;
  try {
    uidInt = BigInt(uid);
  } catch (_) {
    return new Response(`Bad UID "${uid}".`, { status: 400 });
  }

  const dishStatuses = await getDishStatuses(kv, uidInt);
  if (dishStatuses === null) {
    return new Response(`Unknown UID "${uid}".`, { status: 404 });
  }

  const lastUpdatedStr = lastUpdated.toISOString();
  const shortDate = lastUpdatedStr.split("T")[0];

  const entries = [];

  for (const s of dishStatuses) {
    const dishName = s.name;
    // TODO: prevent XSS
    entries.push(`
<entry xml:lang="en">
  <title>${dishName} on ${shortDate}</title>
  <updated>${lastUpdatedStr}</updated>
  <id>${uid}|${dishName}|${lastUpdatedStr}</id>
  <content type="xml" xml:lang="en">${escapeXML(formatMessage(s))}</content>
</entry>`);
  }

  const template = `
<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom" xml:lang="en">
  <title>NCSU Menu Notifier</title>
  <link rel="self" type="application/atom+xml" href="${baseURL}/rss/${uid}/atom.xml"/>
  <updated>${lastUpdatedStr}</updated>
  <author><name>NCSU-MN</name></author>
  <id>${uid}</id>
  ${entries.join("")}
</feed>`;
  console.log(`Serving RSS feed for ${uid}`);
  return new Response(template.trim(), {
    headers: {
      // Must be RFC 7231 format
      "last-modified": lastUpdated.toUTCString(),
      "content-type": "application/atom+xml; charset=utf-8",
      "cache-control": "public; max-age=3600",
    },
  });
}

export async function rssHandler(req: Request, kv: Deno.Kv): Promise<Response> {
  const format = "/rss/:uid/atom.xml";
  const pattern = new URLPattern({ pathname: format });
  if (!pattern.test(req.url)) {
    return new Response(`URL must have the format ${format}.`, { status: 404 });
  }

  const lastUpdated = await getLastUpdated(kv);
  const uid = pattern.exec(req.url)?.pathname.groups.uid as string;
  // Before making more KV lookups, check If-Modified-Since to combat aggressive feed readers
  if (req.headers.has("if-modified-since")) {
    const ims = new Date(req.headers.get("if-modified-since") as string);
    // The IMS header has milliseconds rounded off
    if (lastUpdated.getTime() - ims.getTime() < 1000) {
      console.log(`Returned 304 for ${uid}`);
      return new Response(null, { status: 304 });
    }
  }

  return await generateFeed(kv, uid, lastUpdated, getBaseURL(req.url));
}
