import { storeSignup } from "./storage.ts";

export interface SignupBody {
  uid: string;
  dishes: string[];
}

export async function signupHandler(
  req: Request,
  kv: Deno.Kv,
): Promise<Response> {
  if (req.method !== "POST") {
    return new Response(null, { status: 405 });
  }
  // We store a list of KvKey as the value so we can directly use the
  // keys to look up their ServedStatus.
  const dishKeys: Deno.KvKey[] = [];
  const dishes: string[] = [];
  // The form data contains multiple "favorite_dish" keys where the value is the dish name
  const body = await req.formData();
  body.forEach((value, key, _) => {
    if (key !== "favorite_dish") {
      return new Response("Invalid form data", { status: 400 });
    }
    dishes.push(value.toString());
    dishKeys.push(["dishStatuses", value.toString()]);
  });
  // kv.getMany() is limited to 10 keys.
  if (dishKeys.length > 10) {
    return new Response(
      "Due to technical limitations, you cannot subscribe to more than 10 dishes.",
      { status: 400 },
    );
  }

  const uid = await storeSignup(kv, dishKeys);

  const link = `${getBaseURL(req.url)}/rss/${uid}/atom.xml`;
  console.log(`Processed new signup ${link} : ${dishes}`);
  const resp = (await Deno.readTextFile("public/signup/success.html"))
    .replaceAll("{{ link }}", link);

  return new Response(resp, {
    status: 201,
    headers: {
      "content-type": "text/html; charset=UTF-8",
      "cache-control": "no-store",
    },
  });
}

// I think the request URL can be trusted because both Cloudflare and Deno
// block requests with incorrect Host headers. Django does this with ALLOWED_HOSTS.
// Trusting it avoids the need for setting the base URL in the config, which
// doesn't work for preview environments.
export function getBaseURL(rawURL: string): string {
  const url = new URL(rawURL);
  return `${url.protocol}//${url.host}`;
}
