import { load as cheerioLoad } from "cheerio";
import {
  DiningHall,
  DishLog,
  DishStatus,
  Meal,
  NamedDishLog,
  NamedDishStatus,
  saveDishLog,
  setDishStatus,
  setLastUpdated,
} from "./storage.ts";

interface ScrapedDataEntry {
  statuses: DishStatus[];
  log: NamedDishLog;
}

// When scraping, there are repeats across halls and meals that we have to combine
// using the name as a key. The value is both the DishStatus and a DishLog entry
interface ScrapedDataDict {
  [name: string]: ScrapedDataEntry;
}

// Turns raw HTML into a list of NamedDishLogs. The caller sets the meal and hall.
function getDishLogs(rawHtml: string): NamedDishLog[] {
  const $ = cheerioLoad(rawHtml);
  // All food items are anchors with the 'dining-menu-item-modal' class
  const rawItems = $(".dining-menu-item-modal");
  const dishLogs: NamedDishLog[] = [];

  if (rawItems.length === 0) {
    return dishLogs;
  }
  const lastServed = new Date();
  rawItems.each((_, element) => {
    // Use consistent formatting for the dish's name
    // Remove whitespace, then lowercase the whole string (title case never worked right)
    const elem = $(element);
    const name = elem.text().trim().toLowerCase();
    const traits: string[] = [];
    // More info comes from sibling spans that look like this:
    // <span class="dining-menu-dietary-icon" data-diet="vegan_trait">
    elem.siblings(".dining-menu-dietary-icon").each((_, elem2) => {
      traits.push(elem2.attribs["data-diet"]);
    });
    const dishLog: DishLog = { traits: traits, lastServed: lastServed };
    dishLogs.push({ name: name, log: dishLog });
  });

  return dishLogs;
}

function locationToParam(location: DiningHall): string {
  switch (location) {
    case "Fountain":
      return "&pid=45";
    case "Clark":
      return "&pid=46";
    case "Case":
      return "&pid=47";
    case "University Towers":
      return "&pid=26534";
    default:
      throw new Error(`Invalid location ${location}`);
  }
}

/**
 * Makes a GET request to dining.ncsu.edu to download raw HTML
 * for a given date at a given location, returning the processed data.
 */
async function download(
  date: Date,
  location: DiningHall,
  meal: Meal,
): Promise<ScrapedDataDict> {
  const dateISO = date.toISOString().split("T")[0];
  const baseUrl =
    "https://dining.ncsu.edu/wp-admin/admin-ajax.php?action=ncdining_ajax_menu_results";
  const dateParam = `&date=${dateISO}`;
  const locationParam = locationToParam(location);
  const fullUrl = `${baseUrl}${dateParam}${locationParam}&meal=${meal}`;
  console.log(`Scraping ${location} for ${meal} on ${dateISO}...`);

  const response = await fetch(fullUrl);
  if (!response.ok) {
    console.log(`HTTP error! status: ${response.status}`);
  }
  const htmlText = await response.text();
  // For testing:
  // const htmlText = await Deno.readTextFile(
  //   `src/testdata/${locationParam}_${meal}.html`,
  // );
  // await Deno.writeTextFile(`src/testdata/${locationParam}_${meal}.html`, htmlText);

  const dishLogs: NamedDishLog[] = getDishLogs(htmlText);
  const dishData: ScrapedDataDict = {};

  for (const log of dishLogs) {
    const dishStatus: DishStatus = { meal: meal, diningHall: location };
    const entry: ScrapedDataEntry = { statuses: [dishStatus], log: log };
    Object.assign(dishData, { [log.name]: entry });
  }
  return dishData;
}

/**
 * Main handler function to process all dining halls
 */
export async function downloadHandler(): Promise<void> {
  const downloadDate = new Date();
  const statusData: ScrapedDataDict = {};
  // Make separate calls to download for every dining hall and meal (since it's a new url every time)
  for (const [, diningHall] of Object.entries(DiningHall)) {
    for (const [, meal] of Object.entries(Meal)) {
      try {
        const dataSet = await download(downloadDate, diningHall, meal);
        for (const [key, val] of Object.entries(dataSet)) {
          if (Object.hasOwn(statusData, key)) {
            // Dish already seen today, append to the list of statuses
            statusData[key].statuses = statusData[key].statuses.concat(
              val.statuses,
            );
          } else {
            // Dish hasn't been seen yet, set the ScrapedDataEntry
            statusData[key] = val;
          }
        }
      } catch (e) {
        console.log(e);
      }
    }
  }
  const kv = await Deno.openKv();
  // Save the data in the DB
  for (const [key, val] of Object.entries(statusData)) {
    const nds: NamedDishStatus = { name: key, statuses: val.statuses };
    await setDishStatus(kv, nds);
    await saveDishLog(kv, val.log);
  }
  await setLastUpdated(kv);
}

// For testing:
// downloadHandler();
