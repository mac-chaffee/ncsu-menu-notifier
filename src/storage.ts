// Contains the data storage model
import { BLOCKLIST } from "./blocklist.ts";

enum DiningHall {
  Fountain = "Fountain",
  Clark = "Clark",
  Case = "Case",
  UniversityTowers = "University Towers",
}

enum Meal {
  Breakfast = "breakfast",
  Lunch = "lunch",
  Dinner = "dinner",
  Fastlunch = "fastlunch",
}

export interface DishStatus {
  meal: Meal;
  diningHall: DiningHall;
}

// NamedDishStatus is used for storing and retrieving up-to-date dish info (expires every day)
export interface NamedDishStatus {
  name: string;
  statuses: DishStatus[];
}

// DishLog is used just for saving historical dish data (does not expire)
export interface NamedDishLog {
  name: string;
  log: DishLog;
}

export interface DishLog {
  traits: string[];
  lastServed: Date;
}

// Would be nice if there was an auto-incrementing API or an atomic get-and-set API.
async function getAndIncrementUID(kv: Deno.Kv): Promise<bigint> {
  let res = { ok: false };
  const key = ["globals", "nextUID"];

  let nextUID = 0n;

  while (!res.ok) {
    const nextUIDRes = await kv.get(key);
    if (nextUIDRes.value === null) {
      // Random starting value to make the IDs 4-digit non-round numbers
      nextUID = 1414n;
      res = await kv.atomic().check(nextUIDRes).sum(key, nextUID + 1n).commit();
    } else {
      nextUID = (nextUIDRes.value as Deno.KvU64).value;
      res = await kv.atomic().check(nextUIDRes).sum(key, 1n).commit();
    }
  }
  return nextUID;
}

async function storeSignup(
  kv: Deno.Kv,
  dishKeys: Deno.KvKey[],
): Promise<bigint> {
  const uid = await getAndIncrementUID(kv);
  await kv.set(["uids", uid], dishKeys);
  return uid;
}

async function getDishStatuses(
  kv: Deno.Kv,
  uid: bigint,
): Promise<NamedDishStatus[]> {
  const dishes = (await kv.get(["uids", uid])).value as Deno.KvKey[];
  if (dishes === null) {
    return [];
  }
  const ret: NamedDishStatus[] = [];
  const dishStatuses = (await kv.getMany(dishes)) as Deno.KvEntry<
    DishStatus[]
  >[];

  for (const status of dishStatuses) {
    if (status.value === null) {
      continue; // dishStatuses expire, meaning the dish isn't being served today
    }
    ret.push({ name: status.key[1].toString(), statuses: status.value });
  }
  return ret;
}

/**
 * Saves the serving status in Deno KV, with a TTL so we don't have to manually clear out old entries.
 * Adds the new status to the end of the list if one is present.
 */
async function setDishStatus(
  kv: Deno.Kv,
  dish: NamedDishStatus,
): Promise<void> {
  if (BLOCKLIST.has(dish.name)) {
    return;
  }
  // We download at 3/4AM, so records expire 22 hours later at 1/2AM (depending on DST)
  const ttl = { expireIn: 22 * 60 * 60 * 1000 };
  await kv.set(["dishStatuses", dish.name], dish.statuses, ttl);
}

/**
 * Saves a log of every dish and its "traits" (vegan, allergens, etc.) persistently
 */
async function saveDishLog(kv: Deno.Kv, dish: NamedDishLog): Promise<void> {
  if (BLOCKLIST.has(dish.name)) {
    return;
  }
  await kv.set(["dishLogs", dish.name], dish.log);
}

/**
 * Lists all dishLogs
 */
function listDishLogs(kv: Deno.Kv): Deno.KvListIterator<DishLog> {
  return kv.list({ prefix: ["dishLogs"] }, { consistency: "eventual" });
}

/**
 * Updates the last-modified header when serving the RSS feeds.
 */
async function setLastUpdated(kv: Deno.Kv): Promise<void> {
  await kv.set(["globals", "lastUpdated"], new Date());
}

async function getLastUpdated(kv: Deno.Kv): Promise<Date> {
  let lastUpdated = (await kv.get(["globals", "lastUpdated"])).value as Date;
  if (lastUpdated === null) {
    lastUpdated = new Date(0);
  }
  return lastUpdated;
}

export {
  DiningHall,
  getAndIncrementUID,
  getDishStatuses,
  getLastUpdated,
  listDishLogs,
  Meal,
  saveDishLog,
  setDishStatus,
  setLastUpdated,
  storeSignup,
};
