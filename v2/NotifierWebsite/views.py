from django.conf import settings
from django.views.generic import View
from django.http import HttpResponse, JsonResponse
from django.templatetags.static import static


class LetsEncrypt(View):
    """View for obtaining an SSL cert"""
    def get(self, _):
        return HttpResponse(settings.SSL_KEY)


class ManifestJson(View):
    """View for generating the manifest.json file
    to be used for a progressive web app
    """
    def get(self, _):
        icon_sizes = ["48", "64", "96", "128", "192", "256", "512"]
        icons = [
            {
                "src": static("favicon_{size}.png".format(size=size)),
                "type": "image/png",
                "sizes": "{size}x{size}".format(size=size)
            } for size in icon_sizes
        ]
        data = {
            "short_name": "NCSU-MN",
            "name": "NCSU Menu Notifier",
            "background_color": "#cc0000",
            "theme_color": "#cc0000",
            "start_url": "/",
            "display": "standalone",
            "icons": icons
        }

        return JsonResponse(data)
