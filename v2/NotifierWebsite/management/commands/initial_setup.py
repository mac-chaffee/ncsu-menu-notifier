from datetime import date
from decimal import Decimal
from django.core.management.base import BaseCommand
from django.conf import settings
from ncsumn.models import Meal, DiningHall, Version, MenuItem


class Command(BaseCommand):
    """
    Creates initial DB models
    """
    def handle(self, *args, **options):
        if not settings.DEBUG:
            self.stdout.write(self.style.ERROR('ERROR: DEBUG != True, skipping'))
            return
        Meal.objects.all().delete()
        Meal.objects.create(name="Breakfast")
        Meal.objects.create(name="Brunch")
        Meal.objects.create(name="Lunch")
        Meal.objects.create(name="Dinner")
        self.stdout.write(self.style.SUCCESS('Successfully created Meals'))
        DiningHall.objects.all().delete()
        DiningHall.objects.create(name="Fountain")
        DiningHall.objects.create(name="Clark")
        DiningHall.objects.create(name="Case")
        self.stdout.write(self.style.SUCCESS('Successfully created DiningHalls'))
        MenuItem.objects.all().delete()
        MenuItem.objects.create(
            name='Macaroni',
            calories=Decimal('100.5'),
            is_being_served=True,
        )
        self.stdout.write(self.style.SUCCESS('Successfully created MenuItem'))
        Version.objects.all().delete()
        Version.objects.create(
            version='v1.0.0',
            release_date=date(2016, 10, 12),
            message='NCSU-MN has been update to v1.0.0!'
        )
        Version.objects.create(
            version='v1.1.0',
            release_date=date(2017, 3, 9),
            message='NCSU-MN has been update to v1.0.0!'
        )
        Version.objects.create(
            version='v1.2.0',
            release_date=date(2017, 3, 11),
            message='NCSU-MN has been update to v1.0.0!'
        )
        Version.objects.create(
            version='v1.3.0',
            release_date=date(2017, 4, 13),
            message='NCSU-MN has been update to v1.0.0!'
        )
        Version.objects.create(
            version='v1.4.0',
            release_date=date(2017, 4, 22),
            message='NCSU-MN has been update to v1.0.0!'
        )
        self.stdout.write(self.style.SUCCESS('Successfully created Versions'))
