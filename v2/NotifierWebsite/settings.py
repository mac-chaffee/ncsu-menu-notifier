import os
import sys
from environ import Env


env = Env()
env.read_env()

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
NCSUMN = os.path.join(BASE_DIR, 'ncsumn')
CALORIES = os.path.join(BASE_DIR, 'calories')

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env.str('SECRET_KEY', 'SECRET_KEY')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = env.bool('DJANGO_DEBUG', False)

INTERNAL_IPS = '127.0.0.1'

if DEBUG:
    ALLOWED_HOSTS = ['localhost', '127.0.0.1', '[::1]', 'ncsu-mn-staging.herokuapp.com']
else:
    ALLOWED_HOSTS = ['www.ncsu-mn.com', 'ncsu-mn.com', 'ncsu-menu-notifier.herokuapp.com']

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'debug_toolbar',
    'bootstrap3',
    'gunicorn',
    'rest_framework',
    'ncsumn',
    'scraper',
    'NotifierWebsite',
    'calories',
    'magic_link',
]

MAGIC_LINK = {
    # link expiry, in seconds. If you change this, also change the message in ncsumn/api_views.py
    "DEFAULT_EXPIRY": 3600,
}

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.middleware.gzip.GZipMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'NotifierWebsite.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(NCSUMN, 'templates'), os.path.join(CALORIES, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': env.str('DJANGO_LOG_LEVEL', 'INFO'),
        },
        'test': {
            'handlers': ['console'],
            'level': env.str('DJANGO_LOG_LEVEL', 'INFO'),
        },
        'Twilio': {
            'handlers': ['console'],
            'level': env.str('DJANGO_LOG_LEVEL', 'INFO'),
        },
    },
}
sys.path.insert(1, os.path.dirname(os.path.realpath(__file__)))
WSGI_APPLICATION = 'NotifierWebsite.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases
DATABASES = {
    'default': env.db()
}

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.db.DatabaseCache',
        'LOCATION': 'ncsumn_full_cache_table',
    }
}
# Add this decorator to disable during tests: @override_settings(CACHES=settings.TEST_CACHES)
TEST_CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}

# Authentication
LOGIN_URL = '/login/'
LOGIN_REDIRECT_URL = '/prefs/'


# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Twilio Keys
TWILIO_TEST_ACCOUNT_SID = env.str('TWILIO_TEST_ACCOUNT_SID', '')
TWILIO_TEST_AUTH_TOKEN = env.str('TWILIO_TEST_AUTH_TOKEN', '')
TWILIO_TEST_PHONE_NUMBER = env.str('TWILIO_TEST_PHONE_NUMBER', '')

TWILIO_ACCOUNT_SID = env.str('TWILIO_ACCOUNT_SID', TWILIO_TEST_ACCOUNT_SID)
TWILIO_AUTH_TOKEN = env.str('TWILIO_AUTH_TOKEN', TWILIO_TEST_AUTH_TOKEN)
TWILIO_PHONE_NUMBER = env.str('TWILIO_PHONE_NUMBER', TWILIO_TEST_PHONE_NUMBER)

# Used for alerts; must match a NotifierUser
ADMIN_PHONE_NUMBER = env.str('ADMIN_PHONE_NUMBER', '')

# SSL stuff
SSL_KEY = env.str('SSL_KEY', '')
SSL_URL = env.str('SSL_URL', '')
# Redirect http to https unless in debug mode
SECURE_SSL_REDIRECT = not DEBUG

# For releases
GITLAB_WEBHOOK_TOKEN = env.str('GITLAB_WEBHOOK_TOKEN', '')

# Internationalization
# https://docs.djangoproject.com/en/1.10/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'America/New_York'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.10/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
STATICFILES_DIRS = [os.path.join(NCSUMN, 'static'), os.path.join(CALORIES, 'static')]
# Simplified static file serving. https://warehouse.python.org/project/whitenoise/
STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'
WHITENOISE_KEEP_ONLY_HASHED_FILES = True
