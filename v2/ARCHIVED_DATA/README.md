# Archived data from ncsu-menu-notifier v2

NCSU Menu Notifier has been downloading the menu data from dining.ncsu.edu since 2016, so this data set may be valuable for analysis.

## `nc_state_dining_dish_logs_2021-2025.csv.gz`

This file contains a log of when and where each meal was served in CSV format. You must uncompress the file first using `gzip`.

* Fields: `dish, date, location, meal`
* Earliest entry: 2021-04-13 (dish logging was first implemented on this day)
* Latest entry: 2025-03-01
* Number of rows: 197,199
* Example row: `Fresh Broccoli,2021-04-13,Case,Dinner`
* Notes:
    - Some dish names may be quoted since they contain commas and/or quotes.
    - Some dish names were changed slightly, resulting in duplicate entries.
    - NCSU Menu Notifier only tracks Case, Clark, and Fountain. Dining locations on Centennial Campus and in University Towers were not tracked.
    - NCSU Menu Notifier only tracks Breakfast, Lunch, Dinner, and Brunch. At some point, "Brunch" stopped being an option and "FastLunch" was added, but that is not reflected in this data set.

## `nc_state_dining_all_dishes_2016-2025.csv.gz`

This file contains a list of all dishes that have been served at NC State dining halls since 2016 in CSV format. You must uncompress the file first using `gzip`.

* Fields: `name, allergens, calories, fiber, ingredients, protein, sat_fat, serving_size, sodium,total_carbs, total_fat, trans_fat`
* Earliest entry: Approximately 2016-10-12
* Latest entry: 2025-03-01
* Number of rows: 3,388
* Example row: `Acai Bowl,Vegetarian,196.30,5.80,"Acai Sorbet (Organic Fair Trade Acai Puree, Organic Cane Syrup, Organic Tapioca Syrup, Organic Erythritol, Less Than 0.5% Of: Organic Locust Bean Gum, Organic Guar Gum, Citric Acid, Organic Sunflower Lecithin, Fruit & Vegetable Juice (For Color), Organic Flavor, Carrageenan, Organic Stevia Extract), Banana, Mango Slices (MANGO, WATER, SUGAR, POTASSIUM SORBATE AND SODIUM BENZOATE (PRESERVATIVES), ASCORBIC ACID (VITAMIN C), CITRIC ACID. ), Blueberries, Strawberries, Honey, Chia Seeds",1.50,0.20,SERVING,28.50,43.90,4.80,0.00`
* Notes:
    - Some dish name and ingredient columns may be quoted since they contain commas and/or quotes.
    - Some dish names were changed slightly, resulting in duplicate entries.
    - Not all rows have nutrition information. Nutrition data started being downloaded on 2017-10-14, so dishes that were never served again after that date will definitely not have nutrition data. Some dishes never have nutrition information available.
    - The serving size column often has a placeholder value, but these placeholders are somewhat inconsistent.
    - IMPORTANT: Nutrition data is downloaded the first time a dish is served, then never updated. NC State Dining regularly changes recipes and updates the nutrition information, which unfortunately isn't reflected in this data set.

## `ncsu_mn_favorite_dishes.csv`

This file contains the list of dishes that ncsu-mn users were subscribed to as of 2025-03-01.

* Fields: `dish_name, number_of_subscribers`
* Number of rows: 268
* Example row: `Grandma'S Fried Chicken,12`
* Notes:
    - Some dish names may be quoted since they contain commas and/or quotes.
    - As the official names of dishes changed, some users subscribed to the new dish name without unsubscribing from the old name, resulting in duplicates.


## How the archives were generated

Before the ncsu-mn v2 database was deleted on 2025-03-01, I executed the following SQL commands to save some data. All other data has been deleted to protect the personal information of ncsu-mn's users.

```sql
\COPY (
SELECT ncsumn_menuitem.name AS dish, ncsumn_menuitemlog.date_served AS date, ncsumn_dininghall.name AS location, ncsumn_meal.name AS meal
FROM ncsumn_menuitemlog
INNER JOIN ncsumn_dininghall ON ncsumn_menuitemlog.dining_hall_id = ncsumn_dininghall.id
INNER JOIN ncsumn_meal ON ncsumn_menuitemlog.meal_id = ncsumn_meal.id
INNER JOIN ncsumn_menuitem ON ncsumn_menuitemlog.menu_item_id = ncsumn_menuitem.id
ORDER BY ncsumn_menuitemlog.date_served
) TO './nc_state_dining_dish_logs_2021-2025.csv' WITH (FORMAT csv, DELIMITER ',',  HEADER true);
```

```sql
\COPY (
SELECT name, allergens, calories, fiber, ingredients, protein, sat_fat, serving_size, sodium, total_carbs, total_fat, trans_fat FROM ncsumn_menuitem ORDER BY name
) TO './nc_state_dining_all_dishes_2016-2025.csv' WITH (FORMAT csv, DELIMITER ',',  HEADER true);
```

```sql
\COPY (
SELECT name AS dish_name, count(*) AS number_of_subscribers FROM ncsumn_menuitem INNER JOIN ncsumn_notifieruser_fav_foods ON ncsumn_menuitem.id=menuitem_id GROUP BY name ORDER BY number_of_subscribers DESC
) TO './ncsu_mn_favorite_dishes.csv' WITH (FORMAT csv, DELIMITER ',',  HEADER true);
```

## Other stats

* Number of users: 163
* SMS messages sent/received: 10,611
* Most number of dishes that one person subscribed to: 41
