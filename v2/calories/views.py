import datetime
import json
from django.views.generic import TemplateView, FormView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.shortcuts import reverse
from ncsumn.models import NotifierUser, MenuItem
from calories.forms import CalorieForm
from calories.models import ConsumedMeal, DayOfMeals


class CalorieCounterSignup(TemplateView):
    """Contains a summary of the calorie counter and a button to sign up,
    but only logged-in users can see that button, and only they can POST.
    """
    template_name = "calorie_counter_signup.html"
    title = "Calorie Counter Signup - NCSU Menu Notifier"


@method_decorator(login_required, name='dispatch')
class CalorieGraph(TemplateView):
    """Overview of the user's calorie intake, with a link to fill out
    the calorie counter form
    """
    template_name = "calorie_graph.html"
    title = "Calorie Graph - NCSU Menu Notifier"


@method_decorator(login_required, name='dispatch')
class CalorieFormView(FormView):
    """Allows a user to pick which foods they ate, which creates
    a ConsumedMeal object
    """
    template_name = "calorie_form.html"
    title = "Calorie Form - NCSU Menu Notifier"
    form_class = CalorieForm

    def get_context_data(self, **kwargs):
        """Put the dish id, the number of calories, and the number of servings in
        the context so we can render it with javascript
        """
        kwargs.update(super(CalorieFormView, self).get_context_data(**kwargs))

        calorie_data = {dish.id: (float(dish.calories), dish.serving_size) for dish in MenuItem.objects.filter(
            is_being_served=True,
        )}
        kwargs['calorie_data'] = json.dumps(calorie_data)
        return kwargs

    def get_success_url(self):
        return reverse('calorie_graph')

    def form_valid(self, form):
        # Get the form data
        n_user = NotifierUser.objects.get(auth_user=self.request.user)
        meal = form.cleaned_data.get('pick_meal')
        location = form.cleaned_data.get('pick_location')
        dishes = form.cleaned_data.get('picked_dishes')
        total = form.cleaned_data.get('total')

        try:
            # Create a DayOfMeals if there isn't one yet
            new_day, _ = DayOfMeals.objects.get_or_create(
                user=n_user,
                date=datetime.date.today()
            )

        except DayOfMeals.MultipleObjectsReturned:
            # Someone must've made an extra DayOfMeals on the admin side.
            # Just delete the existing ones. The only valid DayOfMeals are
            # ones created by this method, which will only ever create 1 per day.
            DayOfMeals.objects.filter(
                user=n_user,
                date=datetime.date.today()
            ).delete()
            # Now create the correct one
            new_day = DayOfMeals.objects.create(
                user=n_user,
                date=datetime.date.today()
            )

        # Create the ConsumedMeal object
        new_meal = ConsumedMeal.objects.create(
            day=new_day,
            meal=meal,
            location=location,
            calories=total
        )
        new_meal.dishes.set(dishes)

        # Set new_day's calories for this meal
        new_day.json[meal.name.lower()] += float(total)
        new_day.save()

        return super(CalorieFormView, self).form_valid(form)
