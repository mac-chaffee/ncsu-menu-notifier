import os
from unittest import skipIf

from django.shortcuts import reverse
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.support.ui import Select

from ncsumn.models import MenuItem, Meal, DiningHall
from ncsumn.tests.tests import basic_setup


@skipIf(os.getenv('CI', False), 'Skipping selenium tests in CI')
class TestJS(LiveServerTestCase):
    """Use selenium to test the javascript that powers the calorie form"""
    def setUp(self):
        basic_setup()
        dinner = Meal.objects.get(name='Dinner')
        fountain = DiningHall.objects.get(name='Fountain')

        macaroni = MenuItem.objects.create(name='Macaroni', is_being_served=True)
        macaroni.meals.add(dinner)
        macaroni.locations.add(fountain)

        burritos = MenuItem.objects.create(name='Burritos', is_being_served=True)
        burritos.meals.add(dinner)
        burritos.locations.add(fountain)

        self.driver = webdriver.Firefox()
        # Log in the user by stealing the cookie Django generates and giving it to Selenium
        self.client.login(username='+19195152011', password='test-pass')
        cookie = self.client.cookies['sessionid']
        self.driver.get(self.live_server_url + reverse('prefs'))
        self.driver.add_cookie({'name': 'sessionid', 'value': cookie.value, 'secure': False, 'path': '/'})
        self.driver.refresh()

    def test_change_location(self):
        """Picking a location should display a div of dishes from that location"""
        self.driver.get(self.live_server_url + reverse('calorie_form'))
        self.assertFalse(self.driver.find_element_by_id('Fountain').is_displayed())
        self.assertFalse(self.driver.find_element_by_id('Case').is_displayed())
        self.assertFalse(self.driver.find_element_by_id('Clark').is_displayed())

        location = Select(self.driver.find_element_by_id('id_pick_location'))
        location.select_by_visible_text('Fountain')
        self.assertTrue(self.driver.find_element_by_id('Fountain').is_displayed())

        location.select_by_visible_text('Case')
        self.assertFalse(self.driver.find_element_by_id('Fountain').is_displayed())
        self.assertTrue(self.driver.find_element_by_id('Case').is_displayed())

        location.select_by_visible_text('---------')
        self.assertFalse(self.driver.find_element_by_id('Fountain').is_displayed())
        self.assertFalse(self.driver.find_element_by_id('Case').is_displayed())
        self.assertFalse(self.driver.find_element_by_id('Clark').is_displayed())

    def test_serving_size(self):
        """Selecting a dish should show the serving size div"""
        self.driver.get(self.live_server_url + reverse('calorie_form'))
        location = Select(self.driver.find_element_by_id('id_pick_location'))
        location.select_by_visible_text('Fountain')

        mac_id = MenuItem.objects.get(name='Macaroni').id
        mac_pos = 1  # Burrito is 0, Macaroni is 1
        self.assertFalse(
            self.driver.find_element_by_id(
                'servings_div_for_id_fountain_dishes_%s' % mac_pos
            ).is_displayed()
        )
        self.driver.find_element_by_id('id_fountain_dishes_%s' % mac_pos).click()
        self.assertTrue(self.driver.find_element_by_id('id_fountain_dishes_%s' % mac_pos).is_displayed())
        # Should set the name to be something the backend can parse
        elem = self.driver.find_element_by_id('servings_of_id_fountain_dishes_%s' % mac_pos)
        self.assertEqual('servings_of_%s|fountain' % mac_id, elem.get_attribute('name'))
        # Now un-check macaroni and make sure the name was removed so it doesn't POST
        self.driver.find_element_by_id('id_fountain_dishes_%s' % mac_pos).click()
        elem = self.driver.find_element_by_id('servings_of_id_fountain_dishes_%s' % mac_pos)
        self.assertEqual('', elem.get_attribute('name'))

    def tearDown(self):
        self.driver.close()
