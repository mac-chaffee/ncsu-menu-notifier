from calories.forms import NO_DISHES_ERROR
import datetime
from decimal import Decimal
from django.test import TestCase
from django.shortcuts import reverse
from ncsumn.models import MenuItem, DiningHall, Meal, NotifierUser
from ncsumn.tests.tests import basic_setup
from calories.models import ConsumedMeal, DayOfMeals


class TestCalorieFormView(TestCase):
    def setUp(self):
        basic_setup()

        self.case = DiningHall.objects.get(name='Case')
        self.clark = DiningHall.objects.get(name='Clark')
        self.fountain = DiningHall.objects.get(name='Fountain')

        self.lunch = Meal.objects.get(name='Lunch')

        # Create some dishes being served at various dining halls
        self.macaroni = MenuItem.objects.create(
            name='Macaroni',
            calories=Decimal('100.5'),
            is_being_served=True,
        )
        self.macaroni.locations.add(self.case)

        self.eggplant = MenuItem.objects.create(
            name='Eggplant',
            calories=Decimal('300.5'),
            is_being_served=True,
        )
        self.eggplant.locations.add(self.clark)

        self.pasta = MenuItem.objects.create(
            name='Pasta',
            calories=Decimal('500.5'),
            is_being_served=True,
        )
        self.pasta.locations.add(self.fountain)

    def test_form_valid(self):
        """A valid form should be converted into a ConsumedMeal object"""
        self.assertEqual(0, ConsumedMeal.objects.count())
        self.assertEqual(0, DayOfMeals.objects.count())
        # Test twice to see if the DayOfMeals is being updated correctly
        for i in range(1, 3):
            # Post valid data
            valid_data = {
                'pick_meal': self.lunch.id,
                'pick_location': self.fountain.id,
                'case_dishes': [self.macaroni.id],
                'servings_of_%d|%s' % (self.macaroni.id, 'case'): 1,
                'clark_dishes': [self.eggplant.id],
                'servings_of_%d|%s' % (self.eggplant.id, 'clark'): 1,
                'fountain_dishes': [self.pasta.id],
                'servings_of_%d|%s' % (self.pasta.id, 'fountain'): 1,
                'offset': -50.5
            }
            self.client.login(username='+19195152011', password='test-pass')
            response = self.client.post(reverse('calorie_form'), valid_data)
            # If it was valid, it should redirect to the graph
            self.assertEqual(302, response.status_code)
            self.assertEqual(reverse('calorie_graph'), response.url)
            # It should have created a ConsumedMeal matching valid_data
            self.assertEqual(i, ConsumedMeal.objects.count())
            # It should get_or_create a DayOfMeals (should be only 1 even after this loops)
            self.assertEqual(1, DayOfMeals.objects.count())
            try:
                new_cm = ConsumedMeal.objects.filter(
                    day__user__phone_number='+19195152011',
                    day__date=datetime.date.today(),
                    meal=self.lunch,
                    location=self.fountain
                ).latest('datetime')
                # Calories for this meal = (pasta cals - 50.5) = 450.0
                self.assertEqual(Decimal('450.0'), new_cm.calories)
                # Total calories = 450.0 for every loop, so multiply by i
                self.assertEqual(i * Decimal('450.0'), new_cm.day.json['lunch'])

            except ConsumedMeal.DoesNotExist:
                self.fail("Submitted a valid calorie form, but could not find a matching ConsumedMeal.")

    def test_form_invalid(self):
        """An invalid form should re-render the form and NOT create any ConsumedMeals"""
        self.assertEqual(0, ConsumedMeal.objects.count())
        # Post invalid data
        invalid_data = {
            'pick_meal': self.lunch.id,
            'pick_location': self.fountain.id,
            'fountain_dishes': [],
            'offset': ''
        }
        self.client.login(username='+19195152011', password='test-pass')
        response = self.client.post(reverse('calorie_form'), invalid_data)
        # If it was invalid, it should re-render the form with an error
        self.assertEqual(200, response.status_code)
        self.assertEqual(
            NO_DISHES_ERROR % "Fountain",
            response.context['errors'][0]
        )
        # It should NOT have created a ConsumedMeal
        self.assertEqual(0, ConsumedMeal.objects.count())

    def test_multiple_dayofmeals(self):
        """If more than one DayOfMeals exists for a single user on a single day,
        (like if someone messed with the admin side), form_valid() should
        delete ALL DayOfMeals for that user on that day (since we won't be
        able to tell which DayOfMeals is the right one).
        """
        # Make multiple DayOfMeals for today for +19195152011
        for _ in range(2):
            DayOfMeals.objects.create(
                user=NotifierUser.objects.get(phone_number="+19195152011"),
                date=datetime.date.today()
            )
        self.assertEqual(2, DayOfMeals.objects.count())

        # Post valid data
        valid_data = {
            'pick_meal': self.lunch.id,
            'pick_location': self.fountain.id,
            'case_dishes': [self.macaroni.id],
            'servings_of_%d|%s' % (self.macaroni.id, 'case'): 1,
            'clark_dishes': [self.eggplant.id],
            'servings_of_%d|%s' % (self.eggplant.id, 'clark'): 1,
            'fountain_dishes': [self.pasta.id],
            'servings_of_%d|%s' % (self.pasta.id, 'fountain'): 1,
            'offset': -50.5
        }
        self.client.login(username='+19195152011', password='test-pass')
        response = self.client.post(reverse('calorie_form'), valid_data)
        # Make sure it was valid and sent you to the graph
        self.assertEqual(302, response.status_code)
        self.assertEqual(reverse('calorie_graph'), response.url)

        # Should have deleted those 2 DayOfMeals I made and created the correct one.
        self.assertEqual(1, DayOfMeals.objects.count())
        self.assertAlmostEqual(450.0, DayOfMeals.objects.first().json['lunch'])
