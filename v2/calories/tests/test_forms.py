from decimal import Decimal
from django.test import TestCase
from ncsumn.models import MenuItem, DiningHall, Meal
from ncsumn.tests.tests import basic_setup
from calories.forms import CalorieForm, NO_DISHES_ERROR, NO_SERVINGS_ERROR, POS_SERVINGS_ERROR


class TestCalorieForm(TestCase):
    def setUp(self):
        basic_setup()

        self.case = DiningHall.objects.get(name='Case')
        self.clark = DiningHall.objects.get(name='Clark')
        self.fountain = DiningHall.objects.get(name='Fountain')

        self.lunch = Meal.objects.get(name='Lunch')

        # Create some dishes being served at various dining halls
        self.macaroni = MenuItem.objects.create(
            name='Macaroni',
            is_being_served=True,
        )
        self.macaroni.locations.add(self.case)

        self.eggplant = MenuItem.objects.create(
            name='Eggplant',
            is_being_served=True,
        )
        self.eggplant.locations.add(self.clark)

        self.pasta = MenuItem.objects.create(
            name='Pasta',
            calories=Decimal('100'),
            is_being_served=True,
        )
        self.pasta.locations.add(self.fountain)

    def test_clean_form(self):
        """Since the form uses JS to only show 1 dining hall's meals,
        the clean() method should produce a 'picked_dishes' field
        containing only the dishes matching their 'pick_location'
        """
        # Pretend like the user picked some dishes from each hall before settling on one.
        for hall in [self.case, self.clark, self.fountain]:
            valid_data = {
                'pick_meal': self.lunch.id,
                'pick_location': hall.id,
                'case_dishes': [self.macaroni],
                'servings_of_%d|%s' % (self.macaroni.id, hall.name.lower()): 300,
                'clark_dishes': [self.eggplant],
                'servings_of_%d|%s' % (self.eggplant.id, hall.name.lower()): 1.999,
                'fountain_dishes': [self.pasta],
                'servings_of_%d|%s' % (self.pasta.id, hall.name.lower()): 2.5,
            }
            form = CalorieForm(valid_data)
            self.assertTrue(form.is_valid(), msg=str(form.errors))
            # Should have created 'picked_dishes' containing only 'hall' dishes
            cd = form.cleaned_data
            self.assertEqual(1, len(cd['picked_dishes']))
            expected_dish = MenuItem.objects.get(locations=hall)
            self.assertEqual(expected_dish, cd['picked_dishes'][0])

            # Other data should be correct too
            self.assertEqual(None, cd['offset'])
            self.assertEqual(hall, cd['pick_location'])
            self.assertEqual(self.lunch, cd['pick_meal'])

    def test_clean_invalid_form(self):
        """clean() makes sure you picked >1 dish corresponding to 'pick_location'
        unless you have an offset
        """
        # Test an invalid location
        invalid_data = {
            'pick_meal': self.lunch.id,
            'pick_location': -102,
            'case_dishes': [self.macaroni],
            'servings_of_%d|%s' % (self.macaroni.id, 'case'): 300,
            'clark_dishes': [self.eggplant],
            'servings_of_%d|%s' % (self.eggplant.id, 'clark'): 1.999,
            'fountain_dishes': [self.pasta],
            'servings_of_%d|%s' % (self.pasta.id, 'fountain'): 2.5,
        }
        form = CalorieForm(invalid_data)
        self.assertFalse(form.is_valid())
        self.assertIn('pick_location', form.errors)

        # Test valid data, but with no fountain_dishes and no offset
        invalid_data = {
            'pick_meal': self.lunch.id,
            'pick_location': self.fountain.id,
            'servings_of_%d|%s' % (self.macaroni.id, 'case'): 300,
            'clark_dishes': [self.eggplant],
            'servings_of_%d|%s' % (self.eggplant.id, 'clark'): 1.999,
            'fountain_dishes': [],
        }
        form = CalorieForm(invalid_data)
        self.assertFalse(form.is_valid())
        # Only fountain_dishes should have an error
        self.assertEqual(1, len(form.errors))
        self.assertEqual(
            NO_DISHES_ERROR % "Fountain",
            form.errors['fountain_dishes'][0]
        )

    def test_no_dishes_with_offset(self):
        """You don't have to pick any dishes if you specify an offset.
        This is because someone might not find ANY dish they ate, but they
        still want a rough approximation in the offset
        """
        # No fountain_dishes, but there is an offset
        invalid_data = {
            'pick_meal': self.lunch.id,
            'pick_location': self.fountain.id,
            'servings_of_%d|%s' % (self.macaroni.id, 'case'): 300,
            'clark_dishes': [self.eggplant],
            'servings_of_%d|%s' % (self.eggplant.id, 'clark'): 1.999,
            'fountain_dishes': [],
            'offset': 100.0
        }
        # This should be valid
        form = CalorieForm(invalid_data)
        self.assertTrue(form.is_valid())

    def test_calc_total(self):
        """Make sure the math and the errors are right"""
        valid_data = {
            'pick_meal': self.lunch.id,
            'pick_location': self.fountain.id,
            'case_dishes': [self.macaroni],
            'clark_dishes': [self.eggplant],
            'fountain_dishes': [self.pasta],
            'servings_of_%d|%s' % (self.pasta.id, 'fountain'): 2.5,
        }
        form = CalorieForm(valid_data)
        self.assertTrue(form.is_valid(), msg=str(form.errors))

        # The total should be 250 because the user ate at Fountain
        # so only servings ending in 'fountain' are counted,
        # and 2.5 servings of 100-cal pasta is 250 cals.
        self.assertAlmostEqual(Decimal('250'), form.cleaned_data['total'], 3)

    def test_invalid_cal_total(self):
        """You must specify a positive servings size for each dish you eat"""
        negative_data = {
            'pick_meal': self.lunch.id,
            'pick_location': self.fountain.id,
            'case_dishes': [self.macaroni],
            'clark_dishes': [self.eggplant],
            'fountain_dishes': [self.pasta],
            'servings_of_%d|%s' % (self.pasta.id, 'fountain'): -2.5,
        }
        form = CalorieForm(negative_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(POS_SERVINGS_ERROR, form.errors['__all__'][0])

        missing_data = {
            'pick_meal': self.lunch.id,
            'pick_location': self.fountain.id,
            'case_dishes': [self.macaroni],
            'clark_dishes': [self.eggplant],
            'fountain_dishes': [self.pasta],
            'servings_of_%d|%s' % (self.pasta.id, 'fountain'): '',
        }
        form = CalorieForm(missing_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(NO_SERVINGS_ERROR, form.errors['__all__'][0])
