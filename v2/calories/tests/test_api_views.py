from datetime import date, timedelta
from decimal import Decimal
from rest_framework.test import APITestCase
from django.shortcuts import reverse
from ncsumn.models import MenuItem, DiningHall, Meal, NotifierUser
from ncsumn.tests.tests import basic_setup
from ncsumn.utils import date2datestr
from calories.models import DayOfMeals


class TestCalorieFormView(APITestCase):
    def setUp(self):
        basic_setup()

        self.case = DiningHall.objects.get(name='Case')
        self.clark = DiningHall.objects.get(name='Clark')
        self.fountain = DiningHall.objects.get(name='Fountain')

        self.lunch = Meal.objects.get(name='Lunch')

        # Create some dishes being served at various dining halls
        self.macaroni = MenuItem.objects.create(
            name='Macaroni',
            calories=Decimal('100.5'),
            is_being_served=True,
        )
        self.macaroni.locations.add(self.case)

        self.eggplant = MenuItem.objects.create(
            name='Eggplant',
            calories=Decimal('300.5'),
            is_being_served=True,
        )
        self.eggplant.locations.add(self.clark)

        self.pasta = MenuItem.objects.create(
            name='Pasta',
            calories=Decimal('500.5'),
            is_being_served=True,
        )
        self.pasta.locations.add(self.fountain)

    def test_graph_api(self):
        """calorie_graph_api should return JSON in this format:
        {
            "data": [{"date": ISO8601 string,
                      "breakfast": float,
                      "lunch": float,
                      "dinner": float,
                      "other": float } (0 to 8 times)]
            "upper_bound": ISO8601 string for today,
            "lower_bound": ISO8601 string for one week ago,
        }
        """
        # Create some DayOfMeals to display
        today = date.today()
        DayOfMeals.objects.create(
            user=NotifierUser.objects.get(phone_number="+19195152011"),
            date=today,
            json={
                "breakfast": 10.0,
                "lunch": 30.0,    # Total = 130.0
                "dinner": 40.0,
                "other": 50.0,
            }
        )
        DayOfMeals.objects.create(
            user=NotifierUser.objects.get(phone_number="+19195152011"),
            date=today - timedelta(days=5),
            json={
                "breakfast": 50.0,
                "lunch": 70.0,    # Total = 290.0
                "dinner": 80.0,
                "other": 90.0,
            }
        )
        # This one should not be displayed since it's over a week ago
        DayOfMeals.objects.create(
            user=NotifierUser.objects.get(phone_number="+19195152011"),
            date=today - timedelta(days=9)
        )
        # Get the data
        self.client.login(username='+19195152011', password='test-pass')
        json = self.client.get(reverse('calorie_graph_api')).json()

        # Check the bounds
        self.assertEqual(date2datestr(today - timedelta(weeks=1)), json.get('x_min'))
        self.assertEqual(date2datestr(today), json.get('x_max'))
        self.assertEqual(290.0, json.get('y_max'))

        # Check the data (should be in ascending order)
        expected_data = [
            {
                "date": date2datestr(today - timedelta(days=5)),
                "breakfast": 50.0,
                "lunch": 70.0,
                "dinner": 80.0,
                "other": 90.0,
            },
            {
                "date": date2datestr(today),
                "breakfast": 10.0,
                "lunch": 30.0,
                "dinner": 40.0,
                "other": 50.0,
            }
        ]
        for index, row in enumerate(expected_data):
            self.assertEqual(row, json.get("data")[index])
