"use strict";
// Shows and hides dishes from each dining hall depending on the
// current value of id_pick_location
function showDishes() {
    let element = $("#id_pick_location")[0];
    // Get which index is currently selected
    let index = element.selectedIndex;

    if (index !== 0) {
        // Get the text at that index
        let dining_hall = element.options[index].text;
        // Hide all
        $(".dish-select").hide();
        // Display the one we want
        $(`#${dining_hall}`).show();
        // Always display the offset
        $("#Offset").show();
    } else {
        // Hide all anyway
        $(".dish-select").hide();
    }
}
$("#id_pick_location").change(showDishes);


// Show the serving size field when they pick a food item
// (can pass this method an Event or an Element)
function showServings() {
    let dishID;
    let elemID;
    if (this.hasOwnProperty('currentTarget')){
        dishID = parseInt(this.currentTarget.value);
        elemID = this.currentTarget.id;
    }
    else {
        dishID = parseInt(this.value);
        elemID = this.id;
    }

    // Add or remove the number of servings from the page and post data
    if ($(this).is(":checked")) {
        $(`#servings_div_for_${elemID}`).show();
        let elem = $(`#servings_of_${elemID}`);
        elem.attr("name", `servings_of_${dishID}|${elem.data('location')}`);
    } else {
        $(`#servings_div_for_${elemID}`).hide();
        let elem = $(`#servings_of_${elemID}`);
        elem.attr("name", "");  // A blank name means it won't be posted
    }
}
$("input[type=checkbox]").change(showServings);

// Fill in each serving-block with serving size data from the JSON
function addServings() {
    let servingSize = calorieData[$(this).data('id')][1];
    $(this).append(servingSize);
}

// If the form was invalid, Django re-renders the form with
// your previous data filled in. So we need to show the dishes
// and servings for whatever's selected on page load.
$("document").ready(function() {
    showDishes();
    $(".serving-block").each(addServings);
    $("input[type=checkbox]:checked").each(showServings);
});
