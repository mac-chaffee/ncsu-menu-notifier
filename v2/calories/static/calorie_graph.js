/* my_data looks like:
[
  {
    "date": "2017-06-21T00:00:00-04:00",
    "breakfast": 450.2,
    "lunch": 477.0,
    "dinner": 529.0,
    "other": 120.0,
  }, x8
]
*/
function generate_graph(my_data, x_min, x_max, y_max) {
  // Convert all datestrings into js dates
  my_data.forEach(function(d) {
    d.date = new Date(d.date);
  });
  x_min = new Date(x_min);
  x_max = new Date(x_max);

  // Get the data into stacked-bar format
  var stackify = d3.stack()
    .keys(["breakfast", "lunch", "dinner", "other"]);

  var my_stack = stackify(my_data);

  // Get the dimensions of the outer div
  var width = d3.select(".chart-wrapper").node().getBoundingClientRect().width,
    axisWidth = 40,
    barWidth = (width - axisWidth) / 8;


  // Get the chart svg and set its dimensions
  var chart = d3.select(".chart")
    .attr("width", width)
    .attr("height", 400);

  // Set the max value to either 2500 or the max calories
  var y_max = d3.max([2500, y_max]);

  // Function to scale the y-axis
  var yscale = d3.scaleLinear()
    .domain([0, y_max])
    .range([370, 0]);

  // Create the yaxis from that scale
  var yaxis = d3.axisLeft(yscale);

  // Function to scale the x-axis
  var xscale = d3.scaleTime()
    .domain([x_min, x_max])
    .range([axisWidth, width - barWidth]);

  // Create the x-axis from that scale
  var xaxis = d3.axisBottom(xscale)
    .ticks(d3.timeDay.every(1))
    .tickFormat(d3.timeFormat("%a"));

  // Create each bar and each meal group
  var meal_groups = chart.selectAll("g")
    .data(my_stack).enter()
    .append("g")
      .attr("class", function(d) { return d.key; })
     // For each {0: y0, 1: y1, data: date/b/b/l/d/o}, make a rectangle
    .selectAll("rect")
    .data(function(d) { return d; }).enter()
    .append("g");  // A group to hold the rect and the label

  meal_groups.append("rect")
      .attr("x", function(d) { return xscale(d.data.date); })
      .attr("y", function(d) { return yscale(d[1]); })
      .attr("height", function(d) { return yscale(d[0]) - yscale(d[1]); })
      .attr("width", barWidth)
      .on("mouseover", function(d) {
        // Display the # of calories (it's in a text element in this g)
        $(d3.event.target).next("text").css("opacity", "1")
      })
      .on("mouseout", function(d) {
        $(d3.event.target).next("text").css("opacity", "0")
      });

  meal_groups.append("text")
      .attr("x", function(d) { return xscale(d.data.date) + (barWidth / 2); })
      .attr("y", function(d) { return yscale(d[1]); })
      .attr("dy", ".9em")
      .text(function(d) {
        var cals = (d[1] - d[0]).toFixed(0);
        var height = yscale(d[0]) - yscale(d[1]);
        // Only show a label if there's enough space for one
        return height > 10.3 ? `${cals} cal` : ``
      })
      .style("pointer-events", "none")
      .style("opacity", "0");

  // Add a single line to show the DRV for calories
  // var trendline = chart.append("g")
  //   .attr("class", "trendline")
  //   .attr("transform", `translate(${axisWidth},${yscale(2400)})`)
  //   .append("line")
  //     .attr("x1", 0)
  //     .attr("x2", width - 4)
  //     .attr("stroke", "#A3A3A3")
  //     .attr("stroke-dasharray", "0.2, 6")
  //     .attr("stroke-width", "3")
  //     .attr("stroke-linecap", "round")

  // Display the x-axis
  var renderedAxis = chart.append("g")
    .attr("class", "axis xaxis")
    .attr("transform", `translate(${barWidth / 2},380)`)
    .call(xaxis);
  // Modify the axis
  renderedAxis.select(".domain").remove();
  renderedAxis.selectAll("line").remove();
  renderedAxis.selectAll(".tick").append("circle").attr("r","1.5").attr("fill", "#535353");

  // Display the y-axis
  var yaxis_rendered = chart.append("g")
    .attr("class", "axis yaxis")
    .attr("transform", `translate(${axisWidth},0)`)
    .call(yaxis);

  // Modify the axis
  yaxis_rendered.select(".domain").remove();
  yaxis_rendered.selectAll("line").remove();
  // Change the 0 on the y axis to say "0 cals"
  $(".yaxis > .tick > text:first").text("0 cals");
}
