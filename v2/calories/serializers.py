from rest_framework import serializers
from calories.models import ConsumedMeal


class CalorieSerializer(serializers.ModelSerializer):
    calories = serializers.SerializerMethodField()

    class Meta:
        model = ConsumedMeal
        fields = ('datetime', 'meal', 'location', 'dishes', 'calories')

    def get_calories(self, obj):
        return float(obj.calories)
