from django.db import models
from django.utils import timezone
from django.db.models import JSONField
from ncsumn.models import NotifierUser, Meal, DiningHall, MenuItem


def get_default():
    """Returns the correct default JSON for DayOfMeals.json"""
    return {
        "breakfast": 0.0,
        "lunch": 0.0,
        "dinner": 0.0,
        "other": 0.0,
    }


class DayOfMeals(models.Model):
    """This model exists so that a single query to the DayOfMeals table
    can return the JSON needed for a stacked bar chart in d3.js.
    You'll need to include date.isoformat() in the JSON manually, unfortunately.
    """
    user = models.ForeignKey(NotifierUser, on_delete=models.CASCADE)
    date = models.DateField()
    json = JSONField(default=get_default)  # Default value must be callable

    class Meta:
        ordering = ['-date']
        indexes = [models.Index(fields=['user', 'date'])]
        verbose_name = "day of meals"
        verbose_name_plural = "days of meals"

    def __str__(self):
        return "%s on %s" % (self.user, self.date)


class ConsumedMeal(models.Model):
    """Represents a collection of MenuItems consumed
    by a NotifierUser at a specific place
    """
    day = models.ForeignKey(DayOfMeals, on_delete=models.CASCADE)
    meal = models.ForeignKey(Meal, on_delete=models.CASCADE)
    location = models.ForeignKey(DiningHall, on_delete=models.CASCADE)
    datetime = models.DateTimeField(default=timezone.now)
    dishes = models.ManyToManyField(MenuItem)
    calories = models.DecimalField(max_digits=10, decimal_places=2)

    def __str__(self):
        return "%s at %s (%0.2f cal)" % (self.meal.name, self.location.name, self.calories)
