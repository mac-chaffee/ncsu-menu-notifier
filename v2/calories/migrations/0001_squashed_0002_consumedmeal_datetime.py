# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-09-03 02:03
from __future__ import unicode_literals

import calories.models
import django.contrib.postgres.fields.jsonb
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    replaces = [('calories', '0001_initial'), ('calories', '0002_consumedmeal_datetime')]

    initial = True

    dependencies = [
        ('ncsumn', '0004_notifieruser_cares_about_summer'),
    ]

    operations = [
        migrations.CreateModel(
            name='ConsumedMeal',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('calories', models.DecimalField(decimal_places=2, max_digits=10)),
            ],
        ),
        migrations.CreateModel(
            name='DayOfMeals',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField()),
                ('json', django.contrib.postgres.fields.jsonb.JSONField(default=calories.models.get_default)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ncsumn.NotifierUser')),
            ],
            options={
                'ordering': ['-date'],
            },
        ),
        migrations.AddField(
            model_name='consumedmeal',
            name='day',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='calories.DayOfMeals'),
        ),
        migrations.AddField(
            model_name='consumedmeal',
            name='dishes',
            field=models.ManyToManyField(to='ncsumn.MenuItem'),
        ),
        migrations.AddField(
            model_name='consumedmeal',
            name='location',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ncsumn.DiningHall'),
        ),
        migrations.AddField(
            model_name='consumedmeal',
            name='meal',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ncsumn.Meal'),
        ),
        migrations.AddIndex(
            model_name='dayofmeals',
            index=models.Index(fields=['user', 'date'], name='calories_da_user_id_82ae25_idx'),
        ),
        migrations.AddField(
            model_name='consumedmeal',
            name='datetime',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
