from decimal import Decimal
from django import forms
from ncsumn.models import MenuItem, DiningHall, Meal


NO_SERVINGS_ERROR = "You must specify the number of servings for all dishes you ate"
POS_SERVINGS_ERROR = "You can't eat less than or equal to zero servings of something."
NO_DISHES_ERROR = "You said you ate at %s but didn't pick any dishes from there."


class CalorieForm(forms.Form):
    def __init__(self, *args, **kwargs):
        """Override init so we can add fields for each dining hall dynamically
        instead of hard-coding them
        """
        super(CalorieForm, self).__init__(*args, **kwargs)
        # Add the extra fields
        for hall in DiningHall.objects.all():
            # Need a field to display the dish name
            self.fields['%s_dishes' % hall.name.lower()] = forms.ModelMultipleChoiceField(
                queryset=MenuItem.objects.filter(
                    is_being_served=True,
                    locations=hall
                ),
                label="What did you eat at %s?" % hall.name,
                widget=forms.CheckboxSelectMultiple,
                help_text="These are all the dishes being served today.",
                required=False
            )

    pick_meal = forms.ModelChoiceField(
        queryset=Meal.objects.all().exclude(name='Brunch'),  # Nobody eats brunch anyway
        label="What meal did you eat?",
        help_text="Knowing which meals are calorie-heavy can help you reach your goal.",
        required=True
    )
    pick_location = forms.ModelChoiceField(
        queryset=DiningHall.objects.all(),
        label="Where did you eat?",
        help_text="We'll show you everything they served at that dining hall.",
        required=True
    )
    offset = forms.DecimalField(
        label="Anything else?",
        help_text=("Enter a positive or negative number of calories to fix any "
                   "irregularities in your calorie count for this meal. Like "
                   "if you drank a soda, enter those calories here."),
        required=False
    )
    # A fake field that is filled in manually in the clean method
    total = forms.DecimalField(
        help_text="Total amount of calories for this meal",
        initial=0,
        required=False
    )

    def clean(self):
        """If a user says they ate at Fountain but selected some Case dishes,
        this method will ignore the Case dishes by creating a 'picked_dishes'
        field that the view will use in form_valid().
        """
        cd = super(CalorieForm, self).clean()
        location = cd.get('pick_location')
        if location:
            cd['picked_dishes'] = cd.get('%s_dishes' % location.name.lower())
            cd['total'] = self.calc_total(location.name.lower())

            if not cd.get('picked_dishes') and not cd.get('offset'):
                # Add an error to the dishes of whichever location they picked
                msg = NO_DISHES_ERROR % cd['pick_location'].name
                field = '%s_dishes' % location.name.lower()
                self.add_error(field, msg)
        # Add the offset to the total
        total = cd.get('total')
        offset = cd.get('offset')
        if total and offset:
            cd['total'] = total + offset

        return cd

    def calc_total(self, location_name):
        """Calculate in the total calories based on extra post data.
        The servings will be in the POST data with names like "servings_of_<id>|<dining-hall>"
        """
        total_cals = Decimal()
        for key, val in self.data.items():
            if key.startswith("servings_of_") and key.endswith(location_name):
                dish_id = int(key.lstrip("servings_of_").split("|")[0])
                if not val:
                    raise forms.ValidationError(NO_SERVINGS_ERROR)
                else:
                    servings = Decimal(val)

                if servings <= 0:
                    raise forms.ValidationError(POS_SERVINGS_ERROR)
                try:
                    dish = MenuItem.objects.get(id=dish_id)
                    total_cals += dish.calories * servings
                except MenuItem.DoesNotExist:
                    raise forms.ValidationError("There's something wrong with the ID of that dish. "
                                                "Please report this bug: bugs@ncsu-mn.com")
        return total_cals
