from datetime import date, timedelta
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from ncsumn.models import NotifierUser
from ncsumn.utils import date2datestr
from calories.models import DayOfMeals, ConsumedMeal
from calories.serializers import CalorieSerializer


class CalorieAPI(ListAPIView):
    """Your calories over time"""
    serializer_class = CalorieSerializer

    def get_queryset(self):
        n_user = NotifierUser.objects.get(auth_user=self.request.user)
        return ConsumedMeal.objects.filter(day__user=n_user)


class CalorieGraphAPI(APIView):
    """Calories grouped by day for a stacked bar graph"""

    def get(self, request):
        n_user = NotifierUser.objects.get(auth_user=request.user)
        today = date.today()
        one_week_ago = today - timedelta(weeks=1)

        daily_data = DayOfMeals.objects.filter(
            user=n_user,
            date__range=[one_week_ago, today],
        ).order_by('date')

        # Build a list of dicts for each day of data
        daily_list = []
        max_calories = 0.0
        for day in daily_data:
            # Convert the date into an ISO8601 datetime string at 0:00
            datestr = date2datestr(day.date)
            day_dict = {'date': datestr}
            # Add the json (containing the calories for each meal) to the dict
            day_dict.update(day.json)
            # Add the completed dict to the right spot
            daily_list.append(day_dict)

            # Calculate the max calories while we're at it
            keys = ['breakfast', 'lunch', 'dinner', 'other']
            total_cals = sum([day_dict[key] for key in keys])
            max_calories = total_cals if total_cals > max_calories else max_calories

        # Put the list and the bounds in a single json payload
        payload = {
            "data": daily_list,
            "x_min": date2datestr(one_week_ago),
            "x_max": date2datestr(today),
            "y_max": max_calories
        }

        return Response(payload, status=200)
