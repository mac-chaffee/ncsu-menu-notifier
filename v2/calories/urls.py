from django.conf.urls import url
from calories import views, api_views

urlpatterns = [
    url(regex=r'^calorie_counter_signup/$',
        view=views.CalorieCounterSignup.as_view(),
        name='calorie_counter_signup'),

    url(regex=r'^calories/$',
        view=views.CalorieGraph.as_view(),
        name='calorie_graph'),

    url(regex=r'^calorie_form/$',
        view=views.CalorieFormView.as_view(),
        name='calorie_form'),

    url(regex=r'^calorie_api/$',
        view=api_views.CalorieAPI.as_view(),
        name='calorie_api'),

    url(regex=r'^calorie_graph_api/$',
        view=api_views.CalorieGraphAPI.as_view(),
        name='calorie_graph_api'),
]
