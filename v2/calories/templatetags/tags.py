from django.template import Library
register = Library()


# pylint: disable=unused-argument
@register.simple_tag
def template_pdb(obj):
    """Allows debugging of a template variable"""
    import pdb; pdb.set_trace()
