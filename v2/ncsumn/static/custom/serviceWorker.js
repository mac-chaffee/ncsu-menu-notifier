self.addEventListener('install', function(event) {
  // Perform install steps for a progressive web app
  console.log("Created service worker. It does nothing right now, but in the future, it will cache pages and send notifications");
});
