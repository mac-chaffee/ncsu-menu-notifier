// Show the save buttons when the specific field has changed
$("#id_locations").change(function() {
    $("#save-button-locations").addClass("faded-in");
});

$("#id_meals").change(function() {
    $("#save-button-meals").addClass("faded-in");
});

$("#id_add_dish").change(function() {
    $("#save-button-add").addClass("faded-in");
});

$("#id_wants_updates").change(function() {
    $("#save-button-updates").addClass("faded-in");
});

$("#id_cares_about_summer").change(function() {
    $("#save-button-updates").addClass("faded-in");
});
