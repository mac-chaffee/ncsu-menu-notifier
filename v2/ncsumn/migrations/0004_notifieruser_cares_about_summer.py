# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-05-18 01:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ncsumn', '0003_auto_20170517_2139'),
    ]

    operations = [
        migrations.AddField(
            model_name='notifieruser',
            name='cares_about_summer',
            field=models.BooleanField(default=False),
        ),
    ]
