from rest_framework import serializers
from ncsumn.models import NotifierUser


class NotifierUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = NotifierUser
        fields = ('phone_number', 'verified', 'fav_foods', 'locations', 'meals')
