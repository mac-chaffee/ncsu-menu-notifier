import datetime
import logging
import re

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404, HttpResponse
from rest_framework import status
from rest_framework.generics import RetrieveUpdateAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from twilio.twiml.messaging_response import MessagingResponse
from twilio.request_validator import RequestValidator
from magic_link.models import MagicLink

from ncsumn.models import NotifierUser, Version, SMSMessage
from ncsumn.serializers import NotifierUserSerializer
from ncsumn.utils import translate_list_to_english


class NotifierUserAPI(RetrieveUpdateAPIView):
    """Allows people to view and update their own NotifierUser object"""
    serializer_class = NotifierUserSerializer

    # pylint: disable=arguments-differ
    def get_object(self, phone_number, user):
        """Only let people get their own NotifierUser object"""
        try:
            return NotifierUser.objects.get(phone_number=phone_number, auth_user=user)
        except (TypeError, ObjectDoesNotExist):
            raise Http404

    def retrieve(self, request, phone_number=None):
        """Called after a GET request"""
        item = self.get_object(phone_number, request.user)
        serializer = self.serializer_class(item)
        return Response(serializer.data)

    def update(self, request, **kwargs):
        """Called after a PUT request"""
        phone_number = kwargs.pop('phone_number')
        item = self.get_object(phone_number, request.user)
        serializer = self.serializer_class(item, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def partial_update(self, request, **kwargs):
        """Called after a PATCH request which adds/removes a given dish by id.
        Based loosely on https://tools.ietf.org/html/rfc6902#page-13
        """
        phone_number = kwargs.pop('phone_number')
        item = self.get_object(phone_number, request.user)
        # Parse the data to find the operation and the val to change
        operation = request.data.get('op')
        try:
            food_to_change = int(request.data.get('id'))
        except (TypeError, ValueError):
            return Response({'error': 'The id must be an integer'}, status=status.HTTP_400_BAD_REQUEST)
        old_fav_foods = [food.id for food in item.fav_foods.all()]
        if operation == 'remove':
            data = {'fav_foods': [food_id for food_id in old_fav_foods if food_id != food_to_change]}
        elif operation == 'add':
            data = {'fav_foods': old_fav_foods + [food_to_change]}
        else:
            return Response({'error': 'The operation must be "add" or "remove"'}, status=status.HTTP_405_METHOD_NOT_ALLOWED)
        # Attempt the partial update using 'data'
        serializer = self.serializer_class(item, data=data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class SMSResponse(APIView):
    CONFIRMED_MESSAGE = ("ncsu-mn: You're in! We'll text you when "
                         "your favorite foods are being served!\n"
                         "Change your preferences at ncsu-mn.com")
    BAD_DEMO_MESSAGE = ("You need to have at least one favorite food and "
                        "at least one location to get a demo.")
    LOGIN_MESSAGE = "Use this link to log in (it will expire in 1 hour): {}"
    MAGIC_LINK_EXPIRATION_SECONDS = 3600

    def post(self, request):
        logger = logging.getLogger('Twilio')
        # Validate the request is actually from Twilio
        validator = RequestValidator(settings.TWILIO_AUTH_TOKEN)
        signature = request.headers.get('X-Twilio-Signature', '')
        is_valid = validator.validate(request.build_absolute_uri(), request.data, signature)
        if not is_valid:
            logger.error(f"Received a message with an invalid signature! {request.data=} {request.headers=}")
            return Response(status=204)

        # Get the some values from the POST data
        raw_body = request.data.get('Body', '')
        body = raw_body.upper().strip()
        from_number = request.data.get('From', '')
        twiml = MessagingResponse()

        # Ignore it if the person texting us is some random person
        try:
            user = NotifierUser.objects.get(phone_number=from_number)
        except ObjectDoesNotExist:
            logger.warning("Text from unauthorized number %s ignored", from_number)
            return Response(status=204)

        SMSMessage.objects.create(
            direction="Received",
            user=user,
            message=raw_body
        )

        # Check if the user is verifying
        if body == "CONFIRM":
            user.verified = True
            user.save()
            twiml.message(self.CONFIRMED_MESSAGE)
        # Check if the user wants a demo
        elif body == "DEMO" and user.verified:
            # Basically just do what message_fans does, but for all the user's dishes and locations
            all_foods = [food.name for food in user.fav_foods.all()]
            all_locations = [loc.name for loc in user.locations.all()]

            if all_foods and all_locations:
                twiml.message(
                    "They're serving %s at %s today!" % (translate_list_to_english(all_foods),
                                                         translate_list_to_english(all_locations))
                )
            else:
                twiml.message(self.BAD_DEMO_MESSAGE)
        # Check is the user wants a magic link
        elif body == "LOGIN" and user.verified:
            link = MagicLink.objects.create(user=user.auth_user, redirect_to="/prefs/")
            link_url = request.build_absolute_uri(link.get_absolute_url())
            twiml.message(self.LOGIN_MESSAGE.format(link_url))
        else:
            # Any other message doesn't need a response, so say there's no content
            logger.warning('Unrecognized text "%s" from %s ignored', raw_body, from_number)
            return Response(status=204)

        # Don't log magic links
        cleaned_message = re.sub("https?://.*", "<redacted>", twiml.verbs[0].value)
        SMSMessage.objects.create(
            direction="Sent",
            user=user,
            message=cleaned_message
        )
        logger.info('Replied to %s with "%s"', str(user), cleaned_message)
        return HttpResponse(twiml.to_xml(), content_type='text/xml')


class TagRelease(APIView):
    """Endpoint for the GitLab webhook which triggers when a new tag is pushed.
    This view creates a new Version and prepares to text everyone about it.
    """
    INVALID_TOKEN_MESSAGE = "Invalid X-Gitlab-Token"
    BAD_TAG_MESSAGE = "Could not parse the tag name"

    def post(self, request):
        logger = logging.getLogger('django')
        # Check for the token in the header
        token = request.META.get('HTTP_X_GITLAB_TOKEN')
        if token != settings.GITLAB_WEBHOOK_TOKEN:
            logger.warning(self.INVALID_TOKEN_MESSAGE)
            return Response({"error": self.INVALID_TOKEN_MESSAGE}, status=401)
        # Parse the tag name out of 'ref': refs/tags/v0.0.0
        ref = request.data.get('ref')
        try:
            version = re.search("refs/tags/(.+)", ref).groups()[0]
            is_patch = self.is_patch_update(version)
        except (AttributeError, ValueError):
            logger.warning(self.BAD_TAG_MESSAGE)
            return Response({"error": self.BAD_TAG_MESSAGE}, status=400)

        obj, was_created = Version.objects.update_or_create(
            version=version,
            defaults={'release_date': datetime.date.today()}
        )
        if was_created and not is_patch:
            NotifierUser.objects.all().update(
                is_up_to_date=False
            )
            logger.info("Released new version %s", version)
        else:
            logger.warning("Release %s already exists or is a patch release. No extra texts will be sent.", version)
        return Response({"version": obj.version, "release_date": obj.release_date}, status=200)

    @staticmethod
    def is_patch_update(new_version):
        try:
            old_version = Version.objects.exclude(version__exact=new_version).latest('release_date').version
        except Version.DoesNotExist:
            # There are no other versions, so we can't tell
            return False

        split_old_version = old_version.strip('v').split('.')
        old_major, old_minor, old_patch = map(int, split_old_version)

        split_new_version = new_version.strip('v').split('.')
        new_major, new_minor, new_patch = map(int, split_new_version)

        if new_major == old_major and new_minor == old_minor:
            return True
        return False
