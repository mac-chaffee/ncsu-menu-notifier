from django.conf.urls import url
from ncsumn import views, api_views

urlpatterns = [
    # Regular views
    url(regex=r'^$',
        view=views.Home.as_view(),
        name='home'),

    url(regex=r'^confirm/$',
        view=views.Confirm.as_view(),
        name='confirm'),

    url(regex=r'^prefs/$',
        view=views.Prefs.as_view(),
        name='prefs'),

    url(regex=r'^login/$',
        view=views.PhoneLoginView.as_view(),
        name='login'),

    url(regex=r'^about/$',
        view=views.About.as_view(),
        name='about'),

    url(regex=r'^changelog/$',
        view=views.ChangeLog.as_view(),
        name='changelog'),

    # API views
    url(regex=r'^sms/$',
        view=api_views.SMSResponse.as_view(),
        name='sms'),

    url(regex=r'^notifierusers/(?P<phone_number>\+1[0-9]{10})/$',
        view=api_views.NotifierUserAPI.as_view(),
        name='notifierusers'),

    url(regex=r'tag_release/$',
        view=api_views.TagRelease.as_view(),
        name='tag_release'),
]
