import datetime

from django.test import TestCase

from ncsumn.models import MenuItem, MenuItemLog, Meal, DiningHall
from ncsumn.forms import MENU_ITEMS_WITH_LAST_SERVED
from ncsumn.tests.tests import basic_setup


class TestForms(TestCase):
    def setUp(self):
        basic_setup()

    def test_menu_items_with_last_served(self):
        # Create some MenuItems
        today = datetime.date.today()
        macaroni = MenuItem.objects.create(name='Macaroni')
        MenuItem.objects.create(name='Other Dish')
        lunch = Meal.objects.get(name='Lunch')
        clark = DiningHall.objects.get(name='Clark')
        # Log Macaroni once today and once 10 days ago
        MenuItemLog.objects.create(
            date_served=today,
            menu_item=macaroni, meal=lunch, dining_hall=clark
        )
        MenuItemLog.objects.create(
            date_served=today - datetime.timedelta(days=10),
            menu_item=macaroni, meal=lunch, dining_hall=clark
        )
        self.assertEquals(
            f"Macaroni (last served {today.isoformat()})",
            str(MENU_ITEMS_WITH_LAST_SERVED[0])
        )
        self.assertEquals(
            "Other Dish",
            str(MENU_ITEMS_WITH_LAST_SERVED[1])
        )
