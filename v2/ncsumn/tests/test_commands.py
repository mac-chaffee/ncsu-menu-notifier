from django.db.models.query import prefetch_related_objects
from NotifierWebsite.settings import ADMIN_PHONE_NUMBER
import io
import itertools
import logging
from unittest.mock import patch
from datetime import date
from django.test import TestCase, override_settings
from django.conf import settings
from django.core.management import call_command
from django.contrib.auth.models import User
from ncsumn.models import MenuItem, NotifierUser, Meal, DiningHall, SMSMessage, Version
from ncsumn.utils import translate_list_to_english
from ncsumn.management.commands.message_fans import translate_foods_to_english, is_summer
from freezegun import freeze_time


@override_settings(TWILIO_ACCOUNT_SID=settings.TWILIO_TEST_ACCOUNT_SID,
                   TWILIO_AUTH_TOKEN=settings.TWILIO_TEST_AUTH_TOKEN,
                   TWILIO_PHONE_NUMBER=settings.TWILIO_TEST_PHONE_NUMBER)
class TestCommands(TestCase):
    def setUp(self):
        # Create base objects
        fountain = DiningHall.objects.create(name="Fountain")
        clark = DiningHall.objects.create(name="Clark")
        case = DiningHall.objects.create(name="Case")
        lunch = Meal.objects.create(name="Lunch")
        dinner = Meal.objects.create(name="Dinner")
        # Create several MenuItems with is_being_served=True for lunch items
        macaroni = MenuItem.objects.create(
            name='Macaroni',
            is_being_served=True
        )
        macaroni.meals.add(lunch)
        macaroni.locations.add(fountain)

        potatoes = MenuItem.objects.create(
            name='Potatoes',
            is_being_served=True
        )
        potatoes.meals.add(lunch)
        potatoes.locations.add(fountain)

        burritos = MenuItem.objects.create(
            name='Burritos',
            is_being_served=True
        )
        burritos.meals.add(dinner)
        burritos.locations.add(fountain)

        fajitas = MenuItem.objects.create(
            name='Fajitas',
            is_being_served=True
        )
        fajitas.meals.add(dinner)
        fajitas.locations.add(case)

        spaghetti = MenuItem.objects.create(
            name='Spaghetti',
            is_being_served=True
        )
        spaghetti.meals.add(dinner)
        spaghetti.locations.add(clark)

        # Serve breadsticks at 2 halls
        breadsticks = MenuItem.objects.create(
            name='Breadsticks',
            is_being_served=True
        )
        breadsticks.meals.add(dinner)
        breadsticks.locations.add(clark, fountain)

        # Serve lasagna at all 3 halls
        lasagna = MenuItem.objects.create(
            name='Lasagna',
            is_being_served=True
        )
        lasagna.meals.add(dinner)
        lasagna.locations.add(case, clark, fountain)

        # Create some users who can test different code branches
        self.some_favs = self.create_notifier_user(
            '+19195152011', True,
            [macaroni, burritos], [fountain], [lunch, dinner]
        )
        self.lunch_favs = self.create_notifier_user(
            '+19195152012', True,
            [macaroni, potatoes], [fountain], [lunch, dinner]
        )
        self.dinner_favs = self.create_notifier_user(
            '+19195152013', True,
            [burritos, fajitas], [fountain], [lunch, dinner]
        )
        self.not_verified = self.create_notifier_user(
            '+19195152014', False,
            [macaroni, potatoes], [fountain], [lunch, dinner]
        )
        self.no_favs = self.create_notifier_user(
            '+19195152015', True,
            [], [], [lunch, dinner]
        )

        # Add a user who only cares about specific meals
        self.no_lunch = self.create_notifier_user(
            '+19195152016', True,
            [macaroni, burritos], [fountain], [dinner]
        )
        # Add a user with a dish that's being served for multiple meals and at multiple halls
        cheese_pizza = MenuItem.objects.create(
            name='Cheese Pizza',
            is_being_served=True
        )
        cheese_pizza.meals.add(lunch, dinner)
        cheese_pizza.locations.add(fountain, clark)
        self.multi_hall_and_meal = self.create_notifier_user(
            '+19195152017', True, [cheese_pizza], [fountain, clark], [lunch, dinner]
        )

        Version.objects.create(version='v1.0.0', release_date=date(2016, 10, 12),
                               message="You can now do cool new stuff!")

        self.logger = logging.getLogger('test')

    @staticmethod
    def create_notifier_user(phone_number, verified, fav_foods, locations, meals):
        """Constricts NotifierUsers with the given params"""
        auth_user = User.objects.create(username=phone_number, email='')
        n_user = NotifierUser.objects.create(
            auth_user=auth_user,
            phone_number=phone_number,
            verified=verified
        )
        n_user.fav_foods.add(*fav_foods)
        n_user.locations.add(*locations)
        n_user.meals.add(*meals)
        return n_user

    @override_settings(TWILIO_ACCOUNT_SID=settings.TWILIO_TEST_ACCOUNT_SID,
                       TWILIO_AUTH_TOKEN=settings.TWILIO_TEST_AUTH_TOKEN,
                       TWILIO_PHONE_NUMBER=settings.TWILIO_TEST_PHONE_NUMBER)
    @freeze_time('2017-2-24')
    def test_message_fans(self):
        """message_fans should find which foods are being served for a given
        meal period and text all verified users who have favorited those foods
        """
        out = io.StringIO()
        call_command('message_fans', 'lunch', stdout=out)
        output = out.getvalue()
        self.logger.info(output)
        self._check_logs(output, self.some_favs, 'Macaroni at Fountain')
        self._check_logs(output, self.lunch_favs, 'Macaroni and Potatoes at Fountain')
        # Make sure no_favs, dinner_favs, no_lunch and not_verified didn't get texted
        for user in [self.no_favs, self.dinner_favs, self.not_verified, self.no_lunch]:
            self.assertNotIn(user.phone_number, output)

        # Do the same for dinner
        out = io.StringIO()
        call_command('message_fans', 'dinner', stdout=out)
        output = out.getvalue()
        self.logger.info(output)
        self._check_logs(output, self.some_favs, 'Burritos at Fountain')
        self._check_logs(output, self.dinner_favs, 'Burritos at Fountain')
        # They also favorited fajitas which are being served at a location dinner_favs doesn't use.
        self.assertNotIn('Fajitas at Case', output)
        # Make sure lunch_favs and not_verified didn't get texted
        for user in [self.lunch_favs, self.not_verified]:
            self.assertNotIn(user.phone_number, output)
        # Make sure no_lunch DID get texted this time
        self.assertIn(self.no_lunch.phone_number, output)

    def _check_logs(self, output, user, dishes):
        """Helper method to see if the logs contain something like
        'Telling user.phone_number about dishes'
        """
        expected_log = "Telling %s about %s" % (str(user), dishes)
        self.assertIn(expected_log, output)

    @override_settings(TWILIO_ACCOUNT_SID=settings.TWILIO_TEST_ACCOUNT_SID,
                       TWILIO_AUTH_TOKEN=settings.TWILIO_TEST_AUTH_TOKEN,
                       TWILIO_PHONE_NUMBER=settings.TWILIO_TEST_PHONE_NUMBER)
    def test_update_notifications(self):
        """When there's a new release, user should be told about it ONLY ONCE"""
        # Pretend like lunch_favs hasn't been told about updates yet
        self.lunch_favs.is_up_to_date = False
        self.lunch_favs.save()
        # Run message_fans for lunch so lunch_favs gets texted
        call_command('message_fans', 'lunch')
        # lunch_favs should get the normal notification AND update info
        base_message = "They're serving Macaroni and Potatoes at Fountain for lunch today!"
        extra_update_info = "\nPS: You can now do cool new stuff!\nMore info at www.ncsu-mn.com/changelog/"
        expected_message = base_message + extra_update_info
        actual_message = SMSMessage.objects.filter(user=self.lunch_favs).latest('time').message
        self.assertEqual(expected_message, actual_message)
        # Should be up to date now
        self.lunch_favs.refresh_from_db()
        self.assertTrue(self.lunch_favs.is_up_to_date)

        # Run the command again to send another text to lunch_favs
        call_command('message_fans', 'lunch')
        # lunch_favs should ONLY get the base message
        actual_message = SMSMessage.objects.filter(user=self.lunch_favs).latest('time').message
        self.assertEqual(base_message, actual_message)

        # Now pretend dinner_favs doesn't want to know about updates anymore, but a new update is out
        self.dinner_favs.wants_updates = False
        self.dinner_favs.is_up_to_date = False
        self.dinner_favs.save()
        # Send them a normal text. It shouldn't mention the update
        call_command('message_fans', 'dinner')
        base_message = "They're serving Burritos at Fountain for dinner today!"
        actual_message = SMSMessage.objects.filter(user=self.dinner_favs).latest('time').message
        self.assertEqual(base_message, actual_message)

    def test_is_summer(self):
        """Return true for dates between 5/15 and 7/3"""
        with freeze_time('2090-6-15'):
            self.assertTrue(is_summer())
        with freeze_time('1900-5-30'):
            self.assertTrue(is_summer())
        with freeze_time('2017-7-1'):
            self.assertTrue(is_summer())
        with freeze_time('2017-9-10'):
            self.assertFalse(is_summer())

    @override_settings(TWILIO_ACCOUNT_SID=settings.TWILIO_TEST_ACCOUNT_SID,
                       TWILIO_AUTH_TOKEN=settings.TWILIO_TEST_AUTH_TOKEN,
                       TWILIO_PHONE_NUMBER=settings.TWILIO_TEST_PHONE_NUMBER)
    def test_summer_notifications(self):
        """Over the summer, users should only get notifications if they opt in"""
        # Freeze time so it looks like it's the start of summer
        with freeze_time('2017-5-15'):
            self._check_summer_notifications()

        # Middle of summer
        with freeze_time('2017-6-17'):
            self._check_summer_notifications()

        # End of summer
        with freeze_time('2017-7-3'):
            self._check_summer_notifications()

    @override_settings(TWILIO_ACCOUNT_SID=settings.TWILIO_TEST_ACCOUNT_SID,
                       TWILIO_AUTH_TOKEN=settings.TWILIO_TEST_AUTH_TOKEN,
                       TWILIO_PHONE_NUMBER=settings.TWILIO_TEST_PHONE_NUMBER,
                       ADMIN_PHONE_NUMBER='+19195152015')
    @patch('ncsumn.management.commands.message_fans.send_twilio_message')
    def test_message_fans_error_alert(self, fake_send):
        """On error, message_fans should text the admin"""
        call_command('message_fans', 'some_non_existant_meal')
        admin = NotifierUser.objects.get(phone_number='+19195152015')
        fake_send.assert_called_once_with(admin,
            "Failed to run message_fans(['some_non_existant_meal']): Meal matching query does not exist.")

    def _check_summer_notifications(self):
        """Helper method that checks if messages are being sent during a frozen timezone.now()"""
        # Set defaults
        SMSMessage.objects.all().delete()
        self.lunch_favs.cares_about_summer = False
        self.lunch_favs.save()

        # By default, NO users should be texted over the summer
        call_command('message_fans', 'lunch')
        self.assertFalse(SMSMessage.objects.all().exists(),
                         msg="It's summer, but a non-summer student got texted!")
        # Change that by having lunch_favs opt in
        self.lunch_favs.cares_about_summer = True
        self.lunch_favs.save()

        call_command('message_fans', 'lunch')
        self.assertEqual(1, SMSMessage.objects.filter(user=self.lunch_favs).count(),
                         msg="A summer student didn't get notified of their fav foods")

    def test_translate_foods_to_english(self):
        """Converts a QuerySet of foods to something like
        "Macaroni and Burritos at Fountain and Fajitas at Case"
        """
        # Check 1, 2, and 3 possible halls
        foods = MenuItem.objects.all()
        actual = translate_foods_to_english(foods)
        # There can be multiple right answers since it's hard to sort by len and alpha
        units = [
            "Fajitas at Case",
            "Spaghetti at Clark",
            "Burritos, Macaroni, and Potatoes at Fountain",
            "Breadsticks and Cheese Pizza at Clark and Fountain",
            "Lasagna at Case, Clark, and Fountain"
        ]
        expected_values = []
        # The first 3 units can be in any order, so get all permutations of 1, 2, 3
        perms = set(itertools.permutations(range(3)))
        # Now combine the units into 6 different permutations
        for perm in perms:
            expected_values.append(
                translate_list_to_english([units[val] for val in perm] + [units[3], units[4]])
            )
        # Make sure the actual result was in the expected range of values
        self.assertIn(actual, expected_values)

    def test_translate_list_to_english(self):
        """Test the helper method that converts lists to sentences"""
        long_lst = ['one', 'two', 'three', 'four']
        expected_list = 'one, two, three, and four'
        actual_list = translate_list_to_english(long_lst)
        self.assertEqual(expected_list, actual_list)

        med_lst = ['one', 'two']
        expected_list = 'one and two'
        actual_list = translate_list_to_english(med_lst)
        self.assertEqual(expected_list, actual_list)

        short_lst = ['one']
        expected_list = 'one'
        actual_list = translate_list_to_english(short_lst)
        self.assertEqual(expected_list, actual_list)

        empty_lst = []
        expected_list = ''
        actual_list = translate_list_to_english(empty_lst)
        self.assertEqual(expected_list, actual_list)

    def test_multi_meal_dishes(self):
        """Dishes being served for multiple meals in the same day should
        only appear in the text ONCE
        """
        call_command('message_fans', 'lunch')
        expected_message = "They're serving Cheese Pizza at Clark and Fountain for lunch today!"
        actual_message = SMSMessage.objects.filter(user=self.multi_hall_and_meal).latest('time').message
        self.assertEqual(expected_message, actual_message)
