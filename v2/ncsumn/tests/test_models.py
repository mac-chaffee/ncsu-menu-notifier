from datetime import timedelta

from django.test import TestCase
from django.utils import timezone
from freezegun import freeze_time

from ncsumn.models import NotifierUser, SMSMessage
from ncsumn.tests.tests import basic_setup


class TestModels(TestCase):
    def setUp(self):
        basic_setup()

    def test_should_be_rate_limited(self):
        user = NotifierUser.objects.first()
        for i in range(5):
            self.assertFalse(user.should_be_rate_limited)
            # Only sent messages to should count to the rate limit
            SMSMessage.objects.create(user=user, direction='Sent', message=i)
            SMSMessage.objects.create(user=user, direction='Received', message=i)
        self.assertTrue(user.should_be_rate_limited)
        SMSMessage.objects.all().delete()
        self.assertFalse(user.should_be_rate_limited)

        # Old messages >5 min should not count toward the rate limit
        with freeze_time(timezone.now() - timedelta(minutes=6)):
            for i in range(5):
                SMSMessage.objects.create(user=user, direction='Sent', message=i)
        self.assertFalse(user.should_be_rate_limited)
