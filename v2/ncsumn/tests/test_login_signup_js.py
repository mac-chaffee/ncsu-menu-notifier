import os
import time
from unittest import skipIf

from django.shortcuts import reverse
from django.test import LiveServerTestCase, override_settings
from selenium import webdriver

from ncsumn.forms import BAD_CODE, RATE_LIMITED_ERROR
from ncsumn.models import MenuItem, Meal, DiningHall, NotifierUser, SMSMessage
from ncsumn.tests.tests import basic_setup


@skipIf(os.getenv('CI', False), 'Skipping selenium tests in CI')
class TestJS(LiveServerTestCase):
    """Use selenium to test the javascript that powers the signup and login process"""
    def setUp(self):
        basic_setup()
        dinner = Meal.objects.get(name='Dinner')
        fountain = DiningHall.objects.get(name='Fountain')
        self.macaroni = MenuItem.objects.create(name='Macaroni', is_being_served=True)
        self.macaroni.meals.add(dinner)
        self.macaroni.locations.add(fountain)

        burritos = MenuItem.objects.create(name='Burritos', is_being_served=True)
        burritos.meals.add(dinner)
        burritos.locations.add(fountain)

        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(3)  # Chosen might take a few seconds to load

    @override_settings(DEBUG=True)
    def test_signup_and_login(self):
        # Go to the home page and sign up
        self.driver.get(self.live_server_url)
        self.driver.find_element_by_id("id_phone_number").send_keys('1112223333')
        # Expand the Chosen dropdown
        self.driver.find_element_by_id("id_favorite_dish_chosen").click()
        # Macaroni should be last since it's sorted alphabetically
        elem = self.driver.find_element_by_xpath("//li[@class='active-result'][last()]")
        self.assertIn('Macaroni', elem.text)
        elem.click()
        # Sign up and wait for the page to load
        self.driver.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(1)
        self.assertEqual(self.live_server_url + reverse('confirm'), self.driver.current_url)
        # Verify the user manually
        NotifierUser.objects.filter(phone_number='+11112223333').update(verified=True)

        # Now test the login process
        self.driver.find_element_by_link_text("log in").click()
        self.assertEqual(self.live_server_url + reverse('login'), self.driver.current_url)

        self.assertFalse(self.driver.find_element_by_id("id_password").is_displayed())
        self.driver.find_element_by_id("id_username").send_keys("1112223333")
        self.driver.find_element_by_id("send-code").click()

        # Should hide the "send code" button and show the password field
        self.assertTrue(self.driver.find_element_by_id("id_password").is_displayed())
        self.assertEqual(0, len(self.driver.find_elements_by_xpath("send-code")))

        # An invalid code should display an error message and not redirect
        password_elem = self.driver.find_element_by_id("id_password")
        password_elem.send_keys("invalidcode")
        self.driver.find_element_by_xpath("//button[@type='submit']").click()
        self.assertEqual(self.live_server_url + reverse('login'), self.driver.current_url)
        message = self.driver.find_element_by_xpath("//span[@class='text-muted']").text
        self.assertEqual(BAD_CODE, message)

        # Spamming the button should trigger a rate limit
        password_elem.clear()
        for i in range(5):
            self.driver.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(1)
        self.driver.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(1)  # Extra sleep to wait for the artificial delay
        message = self.driver.find_element_by_xpath("//span[@class='text-muted']").text
        self.assertEqual(RATE_LIMITED_ERROR, message)

        # A valid code should send you to prefs
        password_elem.clear()
        code_message = SMSMessage.objects.latest('time').message
        code = code_message.split()[-1]
        password_elem.send_keys(code)
        self.driver.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(1)
        self.assertEqual(self.live_server_url + reverse('prefs'), self.driver.current_url)

    def tearDown(self):
        self.driver.close()
