import datetime
from decimal import Decimal

from django.conf import settings
from django.test import override_settings, TestCase
from django.contrib.auth.models import User
from django.utils import timezone

from ncsumn.models import NotifierUser, DiningHall, Meal, SMSMessage
from ncsumn.utils import send_twilio_message
from ncsumn.tests.tests import basic_setup
from calories.models import DayOfMeals, ConsumedMeal


@override_settings(TWILIO_ACCOUNT_SID=settings.TWILIO_TEST_ACCOUNT_SID,
                   TWILIO_AUTH_TOKEN=settings.TWILIO_TEST_AUTH_TOKEN,
                   TWILIO_PHONE_NUMBER=settings.TWILIO_TEST_PHONE_NUMBER,
                   DEBUG=False)
class TestUtils(TestCase):
    """NOTE: DEBUG=False for this test so we actually hit the twilio test API"""
    def setUp(self):
        basic_setup()
        # https://www.twilio.com/docs/iam/test-credentials#test-sms-messages-parameters-To
        self.blacklisted_phone = '+15005550004'
        user = User.objects.create(username=self.blacklisted_phone)
        user.set_password('test-pass')
        user.save()
        self.blacklisted = NotifierUser.objects.create(auth_user=user, phone_number=self.blacklisted_phone, verified=True)
        # Create some nasty many-to-many mappings that are hard to delete
        day = DayOfMeals.objects.create(
            user=NotifierUser.objects.get(phone_number="+19195152011"),
            date=datetime.date.today(),
            json={}
        )
        ConsumedMeal.objects.create(
            day=day,
            meal=Meal.objects.first(),
            location=DiningHall.objects.first(),
            calories=Decimal(100)
        )
        SMSMessage.objects.create(time=timezone.now(), direction="Sent", user=self.blacklisted, message="hello")

    def test_send_sms_message(self):
        send_twilio_message(self.blacklisted, "Test message. If you receive this, email bugs@ncsu-mn.com since something broke")
        # Should have deleted the user and corresponding entries
        self.assertEqual(NotifierUser.objects.filter(phone_number=self.blacklisted_phone).count(), 0)
        self.assertEqual(DayOfMeals.objects.filter(user__phone_number=self.blacklisted_phone).count(), 0)
        self.assertEqual(ConsumedMeal.objects.filter(day__user__phone_number=self.blacklisted_phone).count(), 0)
        self.assertEqual(SMSMessage.objects.filter(user__phone_number=self.blacklisted_phone).count(), 0)
