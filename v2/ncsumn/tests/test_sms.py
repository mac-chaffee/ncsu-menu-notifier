import logging

from django.conf import settings
from django.shortcuts import reverse
from django.test import override_settings
from rest_framework.test import APITestCase
from magic_link.models import MagicLink
from twilio.request_validator import RequestValidator

from ncsumn.api_views import SMSResponse
from ncsumn.models import NotifierUser, SMSMessage, MenuItem, DiningHall
from ncsumn.tests.tests import basic_setup


def get_post_args(data):
    """Generates args to self.client.post which includes a valid twilio signature"""
    rv = RequestValidator(settings.TWILIO_TEST_AUTH_TOKEN)
    return {
        'path': reverse('sms'),
        'data': data,
        'HTTP_X_TWILIO_SIGNATURE': rv.compute_signature(f"http://testserver{reverse('sms')}", data)
    }


@override_settings(TWILIO_ACCOUNT_SID=settings.TWILIO_TEST_ACCOUNT_SID,
                   TWILIO_AUTH_TOKEN=settings.TWILIO_TEST_AUTH_TOKEN,
                   TWILIO_PHONE_NUMBER=settings.TWILIO_TEST_PHONE_NUMBER)
class TestSMSResponse(APITestCase):
    """This view is used to verify phone numbers by checking if a known
    phone number sent a "CONFIRM" message.
    """
    def setUp(self):
        # Make a user and notifier user that is NOT verified yet
        basic_setup()
        n_user = NotifierUser.objects.first()
        n_user.verified = False
        n_user.save()
        self.logger = logging.getLogger('test')

    def test_verify(self):
        """Posting any variation of "Confirm" should verify the user and text them back"""
        for valid_msg in ['Confirm', 'cOnFiRm', 'CONFIRM', 'confirm', ' ConFirm', 'ConfIrm ', 'ConfiRm\n']:
            self.assertFalse(NotifierUser.objects.first().verified)

            valid_data = {'Body': valid_msg, 'From': '+19195152011'}
            response = self.client.post(**get_post_args(valid_data))

            self.assertEqual(200, response.status_code)
            self.assertTrue(NotifierUser.objects.first().verified)
            self.assertContains(response, SMSResponse.CONFIRMED_MESSAGE)
            self.assertEqual(SMSResponse.CONFIRMED_MESSAGE, SMSMessage.objects.latest('time').message)

            n_user = NotifierUser.objects.first()
            n_user.verified = False
            n_user.save()

    def test_demo_invalid(self):
        """When a user asks for a demo, it should 204 if they're unverified, or send
        a message explaining that they don't have any fav_foods if they aren't setup yet
        """
        # Asking for a demo when not verified should 204
        data = {'Body': 'DeMo', 'From': '+19195152011'}
        response = self.client.post(**get_post_args(data))
        self.assertEqual(204, response.status_code)

        # Verify the user and try again
        data = {'Body': 'Confirm', 'From': '+19195152011'}
        response = self.client.post(**get_post_args(data))
        self.assertEqual(200, response.status_code)
        self.assertTrue(NotifierUser.objects.first().verified)
        self.assertContains(response, SMSResponse.CONFIRMED_MESSAGE)
        self.assertEqual(SMSResponse.CONFIRMED_MESSAGE, SMSMessage.objects.latest('time').message)

        # This user has no foods, so it should tell them as much
        data = {'Body': 'demO', 'From': '+19195152011'}
        response = self.client.post(**get_post_args(data))
        self.assertEqual(200, response.status_code)
        self.assertContains(response, SMSResponse.BAD_DEMO_MESSAGE)
        self.assertEqual(SMSResponse.BAD_DEMO_MESSAGE, SMSMessage.objects.latest('time').message)

    def test_demo_valid(self):
        """When the user has locations and fav_foods and asks for a demo, send a sample text"""
        # Verify, add a favorite food and location, and try again
        fountain = DiningHall.objects.get(name='Fountain')
        pancakes = MenuItem.objects.create(name='Pancakes')
        pancakes.locations.add(fountain)
        pancakes.save()
        n_user = NotifierUser.objects.first()
        n_user.verified = True
        n_user.fav_foods.add(pancakes)
        n_user.locations.add(fountain)
        n_user.save()

        # Should have a similar message to message_fans
        data = {'Body': 'Demo', 'From': '+19195152011'}
        response = self.client.post(**get_post_args(data))
        self.assertEqual(200, response.status_code)
        expected_str = "They're serving Pancakes at Fountain today!"
        self.assertContains(response, expected_str)
        self.assertEqual(expected_str, SMSMessage.objects.latest('time').message)

    def test_login_valid(self):
        # Verify user for testing
        n_user = NotifierUser.objects.first()
        n_user.verified = True
        n_user.save()
        # Ensure user is sent a valid login link
        data = {'Body': 'loGiN', 'From': '+19195152011'}
        response = self.client.post(**get_post_args(data))
        self.assertEqual(200, response.status_code)
        link = MagicLink.objects.latest('created_at')
        self.assertContains(
            response,
            SMSResponse.LOGIN_MESSAGE.format(f"http://testserver{link.get_absolute_url()}")
        )

    def test_invalid_post(self):
        # Random numbers sending texts should 204.
        invalid_data = {'Body': 'CONFIRM', 'From': '+11111111111'}
        response = self.client.post(**get_post_args(invalid_data))
        self.assertEqual(204, response.status_code)
        self.assertFalse(NotifierUser.objects.first().verified)

        # Messages with bad signature header should be ignored (aka return 204 but don't mark verified)
        response = self.client.post(reverse('sms'), invalid_data, HTTP_X_TWILIO_SIGNATURE='garbage')
        self.assertEqual(204, response.status_code)
        self.assertFalse(NotifierUser.objects.first().verified)
        # Same idea when header is missing
        response = self.client.post(reverse('sms'), invalid_data)
        self.assertEqual(204, response.status_code)
        self.assertFalse(NotifierUser.objects.first().verified)

        invalid_data = {'Body': 'LOGIN', 'From': '+11111111111'}
        response = self.client.post(**get_post_args(invalid_data))
        self.assertEqual(204, response.status_code)
        self.assertFalse(NotifierUser.objects.first().verified)

        # Unrecognized messages from existing users should 204
        invalid_data = {'Body': 'yo what up', 'From': '+19195152011'}
        response = self.client.post(**get_post_args(invalid_data))
        self.assertEqual(204, response.status_code)
        self.assertFalse(NotifierUser.objects.first().verified)
        self.assertEqual('yo what up', SMSMessage.objects.latest('time').message)

        # Verify the user and the result should still be the same
        data = {'Body': 'Confirm', 'From': '+19195152011'}
        response = self.client.post(**get_post_args(data))
        self.assertEqual(200, response.status_code)

        invalid_data = {'Body': 'yo what up', 'From': '+19195152011'}
        response = self.client.post(**get_post_args(invalid_data))
        self.assertEqual(204, response.status_code)
        self.assertTrue(NotifierUser.objects.first().verified)
        self.assertEqual('yo what up', SMSMessage.objects.latest('time').message)
