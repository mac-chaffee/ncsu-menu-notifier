import logging
from datetime import date
from unittest.mock import patch

from bs4 import BeautifulSoup
from django.conf import settings
from django.contrib.auth import get_user
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import reverse
from django.test import TestCase, override_settings
from django.utils.html import escape as html_escape

from ncsumn.forms import NOT_VERIFIED_ERROR, NO_PHONE_ERROR, INVALID_ERROR, BAD_CODE, RATE_LIMITED_ERROR
from ncsumn.models import NotifierUser, SMSMessage, MenuItem, Meal, DiningHall, Version


def basic_setup():
    """Creates an auth User and a NotifierUser, as well as all meals and dining halls"""
    user = User.objects.create(username='+19195152011')
    user.set_password('test-pass')  # Must use set_password so it hashes correctly
    user.save()
    NotifierUser.objects.create(phone_number='+19195152011', auth_user=user, verified=True)
    Meal.objects.create(name='Breakfast')
    Meal.objects.create(name='Brunch')
    Meal.objects.create(name='Lunch')
    Meal.objects.create(name='Dinner')
    DiningHall.objects.create(name="Fountain")
    DiningHall.objects.create(name="Clark")
    DiningHall.objects.create(name="Case")
    Version.objects.all().delete()  # Temporary fix for migrations creating initial versions
    Version.objects.create(version='v1.0.0', release_date=date(2016, 10, 12))


class TestGenericViews(TestCase):
    def setUp(self):
        basic_setup()
        self.logger = logging.getLogger('test')

    def test_all_views(self):
        """Visit every URL and assert that the status code <= 302.
        Just a sanity check.
        """
        self.logger.info('Visiting all normal urls...')
        all_url_names = ['home', 'confirm', 'prefs', 'login', 'about', 'changelog',
                         'ssl', 'manifest', 'favicon']

        for urlname in all_url_names:
            address = reverse(urlname)
            # Visit that url as a logged in user
            self.client.login(username='+19195152011', password='test-pass')
            response = self.client.get(address)
            self.assertTrue(response.status_code <= 302,
                            msg="Could not visit %s, got a %s" % (address, response.status_code))

    def test_ssl_view(self):
        """Make sure settings.SSL_URL returns the string settings.SSL_KEY"""
        response = self.client.get('/' + settings.SSL_URL)
        self.assertEqual(response.content.decode('utf8'), settings.SSL_KEY)


GARBAGE_NUMBERS = [
    '9',
    '123456789',
    '12345678901',
    '99999999999999999999',
    'asd',
    'qwertyuiop',
    '💻💻💻💻💻💻💻💻💻💻',
    '+19195152011'  # Including the country code should fail the validator
]


@override_settings(TWILIO_ACCOUNT_SID=settings.TWILIO_TEST_ACCOUNT_SID,
                   TWILIO_AUTH_TOKEN=settings.TWILIO_TEST_AUTH_TOKEN,
                   TWILIO_PHONE_NUMBER=settings.TWILIO_TEST_PHONE_NUMBER)
class TestLogin(TestCase):
    """Users must log in by inputting their phone number, receiving a code
    via text, and entering that code all within the same view.
    """
    def setUp(self):
        basic_setup()
        # Also create an un-verified user
        user = User.objects.create(username='+19199199119')
        user.set_password('test-pass')  # Must use set_password so it hashes correctly
        user.save()
        NotifierUser.objects.create(phone_number='+19199199119', verified=False, auth_user=user)
        self.logger = logging.getLogger('test')

    def test_bad_login(self):
        """Make sure bad phone numbers throw validation errors and don't send texts.
        Since we're manually POSTing, don't include country codes
        """
        # POST valid phone numbers that don't exist in the system
        non_existant_numbers = ['9999999999', '1234567890']
        self._try_login_with_error(non_existant_numbers, NO_PHONE_ERROR)
        # POST a number that exists, but is un-verified
        unverified_numbers = ['9199199119']
        self._try_login_with_error(unverified_numbers, NOT_VERIFIED_ERROR)
        # POST phone numbers that fail the regex validator
        self._try_login_with_error(GARBAGE_NUMBERS, INVALID_ERROR)

        # Now try posting a valid number with an invalid password
        bad_pass_data = {'username': '9195152011', 'password': '******'}
        response = self.client.post(reverse('login'), data=bad_pass_data)
        self.assertContains(response, BAD_CODE)

    def test_good_login(self):
        """Good phone numbers should not have validation errors and should send texts"""
        # Just post the username, make sure you get a text and you're not logged in
        initial_data = {'username': '9195152011'}
        with patch('ncsumn.forms.get_random_string', return_value='TEST12'):
            response = self.client.post(reverse('login'), data=initial_data)
            self.assertContains(response, "Sent")
            self.assertLogs(logging.getLogger('Twilio'), 'Sent a text to +19195152011')
            self.assertEqual(1, SMSMessage.objects.count())
            self.assertFalse(get_user(self.client).is_authenticated)

        # Now post the username and code. Should log in and send no texts
        all_data = {'username': '9195152011', 'password': 'TEST12'}
        response = self.client.post(reverse('login'), data=all_data)
        self.assertJSONEqual(response.content.decode(), {'url': reverse('prefs')})
        self.assertTrue(get_user(self.client).is_authenticated)

    def test_already_logged_in(self):
        """Login view should send you to prefs if you're already logged in"""
        # GET when not authenticated should show the home screen
        response = self.client.get(reverse('login'))
        self.assertEqual(200, response.status_code)

        # GET when authenticated should redirect to prefs
        self.client.login(username='+19195152011', password='test-pass')
        response = self.client.get(reverse('login'))
        self.assertEqual(302, response.status_code)
        self.assertEqual(reverse('prefs'), response.url)

    def test_multiple_attempts(self):
        """Don't let users send a ton of verification codes by spamming the button"""
        initial_data = {'username': '9195152011'}
        for i in range(5):
            self.client.post(reverse('login'), data=initial_data)
        response = self.client.post(reverse('login'), data=initial_data)
        self.assertJSONEqual(response.content.decode(), {'username': [RATE_LIMITED_ERROR]})

    def test_redirect_to_next(self):
        """Valid logins should return the next url so the JS can do window.location.href"""
        # The next url is taken from the browser's url and placed in a hidden field
        initial_data = {'username': '9195152011', 'next': reverse('calorie_graph')}
        with patch('ncsumn.forms.get_random_string', return_value='TEST12'):
            response = self.client.post(reverse('login'), data=initial_data)
            self.assertJSONEqual(response.content.decode(), {'username': 'Sent'})
            self.assertFalse(get_user(self.client).is_authenticated)

        all_data = {'username': '9195152011', 'password': 'TEST12', 'next': reverse('calorie_graph')}
        response = self.client.post(reverse('login'), data=all_data)
        # Should tell you to go to calorie_graph, not prefs
        self.assertJSONEqual(response.content.decode(), {'url': reverse('calorie_graph')})

    def _try_login_with_error(self, bad_numbers, error):
        """Helper method to try to log in with every number in bad_numbers,
        making sure an error is raised every time
        """
        for number in bad_numbers:
            self.logger.info('Trying to log in with bad number %s', number)
            response = self.client.post(reverse('login'), {'username': number})
            # Hack to handle alternate errors when the number is too long
            if error == INVALID_ERROR and len(number) > 10:
                self.assertContains(response,
                                    html_escape(f'Ensure this value has at most 10 characters (it has {len(number)}).'))
            else:
                self.assertContains(response, error)
        # Bad numbers should not send any texts. All texts are logged in the DB, so check there
        self.assertFalse(SMSMessage.objects.all().exists(),
                         msg='Bad phone numbers should not trigger any text messages')


@override_settings(TWILIO_ACCOUNT_SID=settings.TWILIO_TEST_ACCOUNT_SID,
                   TWILIO_AUTH_TOKEN=settings.TWILIO_TEST_AUTH_TOKEN,
                   TWILIO_PHONE_NUMBER=settings.TWILIO_TEST_PHONE_NUMBER,
                   CACHES=settings.TEST_CACHES)
class TestSignupProcess(TestCase):
    def setUp(self):
        basic_setup()
        dinner = Meal.objects.get(name="Dinner")
        macaroni = MenuItem.objects.create(name='Macaroni')
        macaroni.meals.add(dinner)
        macaroni.save()

    def test_valid_signup(self):
        """POSTing valid data should update_or_create a NotifierUser and a User and verify them"""
        # Post a new phone number and an existing dish
        valid_data = {'phone_number': '9195152000', 'favorite_dish': MenuItem.objects.first().id}
        response = self.client.post(reverse('home'), data=valid_data)
        # Should redirect to confirm, send a text, and create the user
        self.assertEqual(302, response.status_code)
        self.assertEqual(reverse('confirm'), response.url)
        self.assertLogs(logging.getLogger('Twilio'), 'Sent a text to +19195152000')
        try:
            n_user = NotifierUser.objects.get(phone_number='+19195152000')
            auth_user = User.objects.get(username='+19195152000')
        except ObjectDoesNotExist:
            self.fail("Signup should have created a NotifierUser and a User")
        self.assertEqual(auth_user, n_user.auth_user)

        # Post that same, unverified number. Should send another text to re-verify.
        response = self.client.post(reverse('home'), data=valid_data)
        # Should redirect to confirm, send a text, and create the user
        self.assertEqual(302, response.status_code)
        self.assertEqual(reverse('confirm'), response.url)
        self.assertLogs(logging.getLogger('Twilio'), 'Sent a text to +19195152000')
        try:
            n_user = NotifierUser.objects.get(phone_number='+19195152000')
        except ObjectDoesNotExist:
            self.fail("Signing up twice shouldn't delete the NotifierUser")
        # The user should be added to all 3 dining halls and all 4 meals by default
        self.assertEqual(3, n_user.locations.count())
        self.assertEqual(4, n_user.meals.count())
        # The user should have 1 fav_food
        self.assertEqual(1, n_user.fav_foods.count())
        self.assertEqual(valid_data['favorite_dish'], n_user.fav_foods.first().id)

    def test_existing_signup(self):
        """Existing users should be redirected to the login screen"""
        # Post a new phone number and an existing dish
        valid_data = {'phone_number': '9195152011', 'favorite_dish': MenuItem.objects.first().id}
        response = self.client.post(reverse('home'), data=valid_data)
        # Should just redirect to login
        self.assertEqual(302, response.status_code)
        self.assertEqual(reverse('login'), response.url)

    def test_invalid_dish_signup(self):
        """A bad dish ID should show error messages"""
        bad_data = {'phone_number': 9999999999, 'favorite_dish': 9999}
        response = self.client.post(reverse('home'), data=bad_data)
        expected_errors = {
            'favorite_dish':
                ['Select a valid choice. That choice is not one of the available choices.']
        }
        # Make sure the errors are on the form and the response
        self.assertEqual(expected_errors, response.context['form'].errors)
        self.assertContains(response, expected_errors['favorite_dish'][0])

        bad_data = {'phone_number': 9999999999, 'favorite_dish': ''}
        response = self.client.post(reverse('home'), data=bad_data)
        expected_errors = {
            'favorite_dish':
                ['This field is required.']
        }
        self.assertEqual(expected_errors, response.context['form'].errors)
        self.assertContains(response, expected_errors['favorite_dish'][0])

    def test_invalid_phone_signup(self):
        for bad_number in GARBAGE_NUMBERS:
            bad_data = {'phone_number': bad_number, 'favorite_dish': MenuItem.objects.first().id}
            response = self.client.post(reverse('home'), data=bad_data)
            self.assertTrue(response.context['form'].errors)
            self.assertContains(response, 'has-error', count=1, msg_prefix=f'"{bad_number}"')


class TestPrefs(TestCase):
    def setUp(self):
        # Make a NotifierUser and assign them a MenuItem and some Locations.
        basic_setup()
        self.breakfast = Meal.objects.get(name='Breakfast')
        self.lunch = Meal.objects.get(name='Lunch')
        self.dinner = Meal.objects.get(name='Dinner')
        fountain = DiningHall.objects.get(name='Fountain')
        macaroni = MenuItem.objects.create(name='Macaroni')
        macaroni.meals.add(self.lunch)
        macaroni.save()

        self.n_user = NotifierUser.objects.first()
        self.n_user.fav_foods.add(macaroni)
        self.n_user.locations.add(fountain)
        self.n_user.meals.add(self.breakfast, self.lunch)
        self.n_user.save()

        # Make an extra MenuItem so we can change prefs later
        burritos = MenuItem.objects.create(name='Burritos')
        burritos.meals.add(self.lunch)
        burritos.save()

    def test_add_dishes(self):
        # Try to add Burritos
        switch_data = {'add_dish': MenuItem.objects.get(name='Burritos').id}
        self.client.login(username='+19195152011', password='test-pass')
        self.client.post(reverse('prefs'), switch_data, follow=True)
        self.assertIn(MenuItem.objects.get(name='Burritos'),
                      self.n_user.fav_foods.all())

    def test_update_meals(self):
        # Try to add Dinner and remove Breakfast
        switch_data = {'meals': [self.lunch.id, self.dinner.id]}
        self.client.login(username='+19195152011', password='test-pass')
        self.client.post(reverse('prefs'), switch_data, follow=True)
        # Should now have lunch and dinner only
        self.assertIn(self.lunch, self.n_user.meals.all())
        self.assertIn(self.dinner, self.n_user.meals.all())
        self.assertNotIn(self.breakfast, self.n_user.meals.all())

    def test_update_locations(self):
        self.client.login(username='+19195152011', password='test-pass')
        # Make sure the 'Fountain' checkbox starts out as checked="checked". Use beautifulsoup to check.
        response = self.client.get(reverse('prefs'))
        soup = BeautifulSoup(response.content, "html.parser")
        # A "checked" element with this id should exist
        self.assertTrue(soup.find(id="id_locations_2", checked=True))

        # Try to remove Fountain (with all boxes unchecked, empty data is sent)
        remove_data = {'locations': []}
        self.client.post(reverse('prefs'), remove_data, follow=True)
        self.assertEqual(0, self.n_user.locations.count(),
                         msg="The user should have had their locations cleared out.")

        # Try to add Case and Clark
        case_and_clark = DiningHall.objects.all().exclude(name='Fountain').order_by('name')
        add_data = {'locations': [hall.id for hall in case_and_clark]}
        self.client.post(reverse('prefs'), add_data, follow=True)
        self.assertEqual(2, self.n_user.locations.count(),
                         msg="The user should have 2 new locations")

        # Can't compare querysets, so create custom lists to see if the user's locations changed
        expected_locations = [hall.name for hall in case_and_clark]
        actual_locations = [hall.name for hall in self.n_user.locations.all().order_by('name')]
        self.assertEqual(expected_locations, actual_locations,
                         msg="The user's locations should now be 'Case' and 'Clark'")

    def test_update_opt_out(self):
        """Users can opt out of getting notified about site updates"""
        self.client.login(username='+19195152011', password='test-pass')
        self.assertTrue(self.n_user.wants_updates)
        change_data = {'wants_updates': False}
        self.client.post(reverse('prefs'), change_data, follow=True)
        self.n_user.refresh_from_db()
        self.assertFalse(self.n_user.wants_updates)

    def test_update_summer(self):
        """Users can opt out of getting notified over the summer"""
        self.client.login(username='+19195152011', password='test-pass')
        self.assertFalse(self.n_user.cares_about_summer)
        change_data = {'cares_about_summer': True}
        self.client.post(reverse('prefs'), change_data, follow=True)
        self.n_user.refresh_from_db()
        self.assertTrue(self.n_user.cares_about_summer)

    def test_admin_redirect(self):
        """Admins who have already logged in should be sent to
        the admin page if they try to view prefs (since they have no NotifierUser)
        """
        User.objects.create_superuser(username='admin', password='admin', email='admin@admin.com')
        self.client.login(username='admin', password='admin')
        response = self.client.get(reverse('prefs'), follow=True)
        self.assertRedirects(response, reverse('admin:index'))
