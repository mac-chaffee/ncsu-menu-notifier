from django import template
from django.utils.safestring import mark_safe
from mistune import markdown

register = template.Library()


@register.simple_tag
def render_changelog():
    """Opens CHANGELOG.md, renders it from markdown to html, and returns it"""
    with open('CHANGELOG.md', 'r') as changelog:
        return mark_safe(markdown(changelog.read()))
