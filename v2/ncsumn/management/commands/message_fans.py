import string
import datetime
import traceback
from collections import OrderedDict
from django.core.management.base import BaseCommand
from django.conf import settings
from ncsumn.models import NotifierUser, Meal, Version
from ncsumn.utils import send_twilio_message, translate_list_to_english


class Command(BaseCommand):
    help = 'Let people know if their favorite food is being served.' \
           'Must specify either breakfast, lunch, or dinner'

    def add_arguments(self, parser):
        parser.add_argument('meal', nargs=1, type=str)

    def handle(self, *args, **options):
        try:
            meal = Meal.objects.get(name=options['meal'][0].title())
            # Loop over the verified users who care about this meal
            for user in NotifierUser.objects.filter(verified=True, meals__in=[meal]):
                # Don't text users if it's the summer and they don't care about it
                if is_summer() and not user.cares_about_summer:
                    continue
                # Only get foods being served today for the specified meal.
                # Note: meals__name looks wrong, but it does actually check if the meal is in meals
                food_today = user.fav_foods.filter(is_being_served=True,
                                                meals__name=meal.name.title(),
                                                locations__in=user.locations.all()).distinct()
                if food_today:
                    # Need to send a single, natural-sounding text for all food items
                    food_string = translate_foods_to_english(food_today)
                    msg = "They're serving %s for %s today!" % (food_string, meal.name.lower())
                    # Attach a link to the changelog if they haven't been texted about the update yet
                    if not user.is_up_to_date and user.wants_updates:
                        version_message = Version.objects.latest('release_date').message
                        msg += "\nPS: %s\nMore info at www.ncsu-mn.com/changelog/" % version_message
                        user.is_up_to_date = True
                        user.save()
                    self.stdout.write(self.style.SUCCESS('Telling %s about %s' % (user.__str__(), food_string)))
                    send_twilio_message(user, msg)
        except Exception as err:
            message = f"Failed to run message_fans({options['meal']}): {err}"
            tb = ''.join(traceback.format_exception(None, err, err.__traceback__))
            self.stdout.write(self.style.ERROR(message))
            self.stdout.write(self.style.ERROR(tb))
            admin = NotifierUser.objects.get(phone_number=settings.ADMIN_PHONE_NUMBER)
            send_twilio_message(admin, message)


def is_summer():
    """Checks if today falls between 2 hardcoded dates that generally
    correspond to the start and end of summer (safe enough for 2017-2019)
    """
    # The comparison is year-insensitive, so set the year to the current year.
    today = datetime.date.today()
    summer_start = datetime.date(today.year, 5, 15)
    summer_end = datetime.date(today.year, 7, 3)
    return summer_start <= today <= summer_end


def translate_foods_to_english(food_today):
    """Given a QuerySet of foods, returns a natural-sounding string
    of those foods at their given locations.
    Ex:
    Input: [<Macaroni (Fountain)>, <Burritos (Fountain)>, <Fajitas (Case)>]
    Output: "Macaroni and Burritos at Fountain and Fajitas at Case"
    """
    # We need to create a bunch of strings with this format, where halls and
    # foods must not be repeated between templates.
    template = string.Template("${foods} at ${locations}")
    # Put all possible locations into a UNIQUE list of tuples (sorted for consistency)
    # Must use tuples so that set can hash each tuple to remove duplicates
    location_combos = sorted(set([tuple(sorted([loc.name for loc in item.locations.all()])) for item in food_today]))
    # Now associate each food with one of these hall combos. Good use for an OrderedDict
    unsorted_dict = {location: [] for location in location_combos}
    # The lambda just sorts the associations by len(key)
    associations = OrderedDict(sorted(unsorted_dict.items(), key=lambda t: len(t[0])))
    for food in food_today:
        for location in location_combos:
            if location == tuple(sorted([loc.name for loc in food.locations.all()])):
                associations[location].append(food.name)
    # Combine each food/hall association into the templates
    templates = []
    for locations, foods in associations.items():
        # Translate locations and foods to english
        translated_locations = translate_list_to_english(locations)
        translated_foods = translate_list_to_english(sorted(foods))
        templates.append(
            template.substitute(
                locations=translated_locations,
                foods=translated_foods
            )
        )
    # Now combine all templates into a single string
    return translate_list_to_english(templates)
