from django.contrib import messages
from django.contrib.auth import login as auth_login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth.views import LoginView
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import FormView, TemplateView

from ncsumn.forms import (SignupForm, LoginForm, PrefsForm)
from ncsumn.models import NotifierUser, DiningHall, Meal
from ncsumn.utils import send_twilio_message


class Home(FormView):
    template_name = 'home.html'
    form_class = SignupForm
    title = "Signup - NCSU Menu Notifier"

    def get_success_url(self):
        return reverse('confirm')

    def form_valid(self, form):
        cd = form.cleaned_data
        # Create a new user if needed and add the food to their favorites
        full_number = "+1" + cd['phone_number']
        auth_user = User.objects.get_or_create(username=full_number)[0]
        user, was_created = NotifierUser.objects.get_or_create(phone_number=full_number, auth_user=auth_user)
        msg = "ncsu-mn: Are you sure you want to sign up for NCSU " \
              "menu notifications?\nReply 'Confirm' if so."
        if was_created:
            # Set their initial preferences
            user.fav_foods.add(cd['favorite_dish'])
            user.locations.add(*DiningHall.objects.all())
            user.meals.add(*Meal.objects.all())
            # Verify the user
            send_twilio_message(user, msg)
        else:
            if not user.verified:
                # Verify the user again, but add more info to the message
                send_twilio_message(user, msg + "\nIf you did not request this, you can reply STOP.")
                user.fav_foods.add(cd['favorite_dish'])
            else:
                messages.info(self.request, "You've already signed up. Please log in to configure your account.")
                return HttpResponseRedirect(reverse('login'))

        # Call the parent method to redirect to success_url
        return super(Home, self).form_valid(form)


class Confirm(TemplateView):
    template_name = 'confirm.html'
    title = "Confirm - NCSU Menu Notifier"


class About(TemplateView):
    template_name = 'about.html'
    title = "About - NCSU Menu Notifier"


class ChangeLog(TemplateView):
    template_name = 'changelog.html'
    title = "Changes - NCSU Menu Notifier"


class PhoneLoginView(LoginView):
    """Override form_invalid and form_valid to return JSON"""
    template_name = 'login.html'
    title = "Login - NCSU Menu Notifier"
    form_class = LoginForm
    redirect_authenticated_user = True

    def form_valid(self, form):
        """If there's no password (code), return "Sent" to show that a message
        was sent in the form's clean() method.
        Otherwise, log the user in.
        """
        if form.cleaned_data.get('password'):
            auth_login(self.request, form.get_user())
            # Remove their temporary password
            form.get_user().set_unusable_password()
            # Normally you'd redirect to get_success_url, but AJAX will follow that and waste time
            # processing the response before forwarding, so just return the success url in json
            return JsonResponse({'url': self.get_success_url()})
        # Pretend like the 'username' field has an error so the JS can render it
        return JsonResponse({'username': 'Sent'})

    def form_invalid(self, form):
        """Need custom behavior for async form validation"""
        # Only return json when the error is in the username
        return JsonResponse(form.errors)


@method_decorator(login_required, name='dispatch')
class Prefs(FormView):
    template_name = 'prefs.html'
    title = "User Preferences - NCSU Menu Notifier"
    form_class = PrefsForm

    def get(self, request, *args, **kwargs):
        if self.request.user.is_staff:
            return HttpResponseRedirect(reverse('admin:index'))
        return super(Prefs, self).get(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('prefs')

    def get_context_data(self, **kwargs):
        """Put all the user's foods in the context"""
        notifier_user = NotifierUser.objects.get(phone_number=self.request.user.username)
        kwargs = super(Prefs, self).get_context_data(**kwargs)
        kwargs['fav_foods'] = notifier_user.fav_foods.all()
        return kwargs

    def get_initial(self):
        """Set the user's initial locations and meals"""
        notifier_user = NotifierUser.objects.get(phone_number=self.request.user.username)
        initial_vals = {
            'locations': [l.id for l in notifier_user.locations.all()],
            'meals': [m.id for m in notifier_user.meals.all()],
            'wants_updates': notifier_user.wants_updates,
            'cares_about_summer': notifier_user.cares_about_summer
        }
        return initial_vals

    def form_valid(self, form):
        cd = form.cleaned_data
        phone_number = self.request.user.username
        notifier_user = NotifierUser.objects.get(phone_number=phone_number)

        # Add any dish they want to add
        dish_to_add = cd.get('add_dish')
        if dish_to_add:
            notifier_user.fav_foods.add(dish_to_add)

        # Change their preferred locations
        updated_locations = cd.get('locations')
        notifier_user.locations.clear()
        for loc in updated_locations:
            notifier_user.locations.add(loc)

        # Change their preferred meals
        updated_meals = cd.get('meals')
        notifier_user.meals.clear()
        for meal in updated_meals:
            notifier_user.meals.add(meal)

        # Change their other preferences
        notifier_user.wants_updates = cd.get('wants_updates')
        notifier_user.cares_about_summer = cd.get('cares_about_summer')

        notifier_user.save()

        return super(Prefs, self).form_valid(form)
