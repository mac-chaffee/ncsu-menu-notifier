from django.conf import settings
from django.contrib import admin
from ncsumn.models import NotifierUser, MenuItem, SMSMessage, Meal, Version
from ncsumn.utils import translate_list_to_english


class InlineNotifierUser(admin.StackedInline):
    model = NotifierUser.fav_foods.through
    extra = 0


@admin.register(MenuItem)
class MenuItemAdmin(admin.ModelAdmin):
    list_display = ['name', 'get_meals', 'get_locations', 'num_fans']
    search_fields = ['name']
    inlines = [InlineNotifierUser]

    def get_meals(self, obj):
        """Returns a string of meals"""
        return translate_list_to_english([m.name for m in obj.meals.all()])
    get_meals.short_description = 'Meals'

    def get_locations(self, obj):
        """Returns a string of locations"""
        return translate_list_to_english([l.name for l in obj.locations.all()])
    get_locations.short_description = 'Locations'


@admin.register(SMSMessage)
class SMSMessageAdmin(admin.ModelAdmin):
    list_display = ['time', 'user', 'message']
    search_fields = ['user', 'message']


@admin.register(Meal)
class MealAdmin(admin.ModelAdmin):
    pass


@admin.register(Version)
class VersionAdmin(admin.ModelAdmin):
    list_display = ['version', 'release_date']


@admin.register(NotifierUser)
class NotifierUserAdmin(admin.ModelAdmin):
    list_display = ['phone_number', 'get_favs']
    search_fields = ['phone_number']

    def get_favs(self, obj):
        """Returns a string of MenuItems"""
        return translate_list_to_english([f.name for f in obj.fav_foods.all()])
    get_favs.short_description = "Favorite Foods"


if settings.DEBUG:
    admin.site.site_header = '***STAGING ADMIN***'
    admin.site.site_title = '***STAGING ADMIN***'
else:
    admin.site.site_header = 'NCSU Menu Notifier'
    admin.site.site_title = 'NCSU Menu Notifier'
