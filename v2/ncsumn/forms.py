from django import forms
from ncsumn.models import MenuItem, NotifierUser, DiningHall, Meal, MenuItemLog
from django.core.validators import RegexValidator
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.contrib.auth import authenticate
from django.contrib.auth.forms import AuthenticationForm
from django.db.models import OuterRef, Subquery
from ncsumn.utils import send_twilio_message
from django.utils.crypto import get_random_string


NOT_VERIFIED_ERROR = "Your phone hasn't been verified yet."
NO_PHONE_ERROR = "Your phone number is not in the system. You need to sign up first."
INVALID_ERROR = "Phone number must be entered in the format: '9999999999'."
RATE_LIMITED_ERROR = "You've already been sent multiple verification codes. Please wait for the messages to arrive."
BAD_CODE = "The code you entered was invalid."
PHONE_REGEX = r'^\d{10}$'

MENU_ITEMS_WITH_LAST_SERVED = MenuItem.objects.all().only('name').annotate(
    last_served=Subquery(
        MenuItemLog.objects.filter(menu_item_id=OuterRef('id'))
            .order_by('-date_served',)
            .values('date_served')[:1])
    ).order_by('name')


class SignupForm(forms.Form):
    phone_regex = RegexValidator(regex=PHONE_REGEX,
                                 message=INVALID_ERROR)
    phone_number = forms.CharField(validators=[phone_regex], help_text="(e.g. 9195152011)", max_length=10)
    favorite_dish = forms.ModelChoiceField(
        queryset=MENU_ITEMS_WITH_LAST_SERVED,
        help_text="This is the list of all dishes served in the last 5 years."
    )

    def clean(self):
        raise ValidationError("New signups are no longer accepted. Please see the changelog.")


class LoginForm(AuthenticationForm):
    phone_regex = RegexValidator(regex=PHONE_REGEX, message=INVALID_ERROR)
    # The username is the phone number without the country code
    username = forms.CharField(
        validators=[phone_regex], help_text="(e.g. 9195152011)", max_length=10,
        widget=forms.TextInput(attrs={'placeholder': 'Phone Number'})
    )
    # The password is their verification code
    password = forms.CharField(
        max_length=6, required=False,
        widget=forms.TextInput(attrs={'placeholder': 'Code'})
    )

    def clean_username(self):
        """
        The phone number must correspond to an existing, verified NotifierUser
        """
        raise ValidationError("New logins are no longer accepted. Please see the changelog.")
        number = "+1" + self.cleaned_data['username']
        try:
            user = NotifierUser.objects.get(phone_number=number)
            if not user.verified:
                raise forms.ValidationError(NOT_VERIFIED_ERROR)
        except ObjectDoesNotExist:
            raise forms.ValidationError(NO_PHONE_ERROR)
        return number

    def clean(self):
        """
        If both fields are valid, try to login the user.
        If code is blank, send a text.
        """
        phone_number = self.cleaned_data.get('username')
        code = self.cleaned_data.get('password')
        if phone_number is not None and code:
            # Try to log in
            self.user_cache = authenticate(username=phone_number, password=code)
            if self.user_cache is None:
                raise forms.ValidationError(
                    BAD_CODE,
                    code='invalid_login',
                    params={'username': phone_number},
                )
            else:
                self.confirm_login_allowed(self.user_cache)
        elif phone_number:
            notifieruser = NotifierUser.objects.get(phone_number=phone_number)
            if notifieruser.should_be_rate_limited:
                raise forms.ValidationError({'username': RATE_LIMITED_ERROR})
            # Set the user's password to a random code and text them that code
            code = get_random_string(6, 'bcdfghjkmnpqrstvwxz123456789')
            notifieruser.auth_user.set_password(code)
            notifieruser.auth_user.save()
            msg = "ncsu-mn: Your verification code is %s" % code
            send_twilio_message(notifieruser, msg)

        return self.cleaned_data


class PrefsForm(forms.Form):
    locations = forms.ModelMultipleChoiceField(
        queryset=DiningHall.objects.all().order_by('name'),
        label="Dining halls you care about",
        help_text=("You can specify which dining halls you want to be notified about. For example, "
                   "if they serve one of your favorite foods at Clark, but you have Clark "
                   "unchecked, we won't text you about it."),
        widget=forms.CheckboxSelectMultiple,
        required=False
    )
    meals = forms.ModelMultipleChoiceField(
        queryset=Meal.objects.all().exclude(name='Other').order_by('name'),
        label="Meals you care about",
        help_text="If you're a late riser, you probably should uncheck \"Breakfast\".",
        widget=forms.CheckboxSelectMultiple,
        required=False
    )
    wants_updates = forms.BooleanField(
        label="I want to know when ncsu-mn gets updated",
        help_text=("If this box is checked and the site gets updated, we'll add a short message "
                   "to the end of one of your normal notifications which tells you about new features."),
        required=False,
    )
    cares_about_summer = forms.BooleanField(
        label="I want to keep receiving notifications over the summer",
        help_text="If you use the dining halls over the summer, you might want to check this box.",
        required=False,
    )
    add_dish = forms.ModelChoiceField(
        label="Add a new dish",
        help_text=("We'll text you when they're serving this dish (as well as any of your "
                   "existing dishes) at one of your specified dining halls."),
        queryset=MENU_ITEMS_WITH_LAST_SERVED,
        required=False
    )
    # Dishes are deleted asynchronously via the API view
