import logging
from datetime import datetime, time
from django.utils.timezone import make_aware
from django.utils import timezone
from django.conf import settings
from twilio.rest import Client
from twilio.base.exceptions import TwilioRestException
from ncsumn.models import SMSMessage, NotifierUser


def send_twilio_message(user: NotifierUser, body: str):
    """Sends a text with the given body to the phone number
    in user.phone_number (only if DEBUG=False)
    """
    logger = logging.getLogger('Twilio')
    client = Client(settings.TWILIO_ACCOUNT_SID,
                    settings.TWILIO_AUTH_TOKEN)
    # Print the message if in debug mode
    if settings.DEBUG:
        logger.info('Fake Message to %s: %s', str(user), body)
    else:
        try:
            client.messages.create(
                body=body,
                to=user.phone_number,
                from_=settings.TWILIO_PHONE_NUMBER
            )
        except TwilioRestException as err:
            # https://www.twilio.com/docs/api/errors/21610
            if err.code == 21610:
                user.delete()
                logger.warning(f"User {user} has opted out of notifications. Purged from ncsu-mn.")
                return
            else:
                raise

        logger.info('Sent a text to %s', str(user))

    # Always log the message
    SMSMessage.objects.create(time=timezone.now(),
                              direction="Sent",
                              user=user,
                              message=body)


def translate_list_to_english(lst):
    """Given a list of dicts, returns one long 'english' string.
    ['red', 'green', 'blue'] becomes 'red, green, and blue'
    :param lst: A list to translate
    :return: A plain-speech version of that list
    """
    final_string = ''
    if len(lst) == 2:
        final_string += lst[0]
        final_string += " and "
        final_string += lst[1]
    elif len(lst) == 1:
        final_string += lst[0]
    elif not lst:
        final_string = ''
    else:
        for i in range(0, len(lst) - 1):
            final_string += lst[i]
            final_string += ", "
        final_string += "and "
        final_string += lst[len(lst) - 1]
    return final_string


def date2datestr(date):
    """Converts a date object into a timezone-aware datetime at 0:00,
    then converted to a string in ISO8601 format
    """
    return make_aware(datetime.combine(date, time.min)).isoformat()
