
import os
import html
import requests
import datetime
import traceback
from bs4 import BeautifulSoup
from decimal import Decimal, InvalidOperation
from django.core.management.base import BaseCommand
from django.conf import settings
from ncsumn.models import MenuItem, Meal, DiningHall, MenuItemLog, NotifierUser
from ncsumn.utils import send_twilio_message


ALL_MEALS = ['Breakfast', 'Lunch', 'Dinner', 'Brunch']
ALL_HALLS = ['Fountain', 'Clark', 'Case']


class Command(BaseCommand):
    help = 'Scrape the NCSU menu and save the results to the database'

    def handle(self, *args, **options):
        try:
            # Before doing anything, reset the meal, dining_hall, and is_being_served
            self.stdout.write(self.style.WARNING('Resetting all MenuItems to defaults for the new day...'))
            for item in MenuItem.objects.all():
                item.is_being_served = False
                for meal in item.meals.all():
                    item.meals.remove(meal)
                for hall in item.locations.all():
                    item.locations.remove(hall)
                item.save()

            # Make separate calls to scraper_script for every dining hall (since it's a new url every time)
            for dining_hall in DiningHall.objects.all():
                data = self.scrape(datetime.date.today(), dining_hall.name)
                # Load all the menu items into the DB, or update them if they're already there
                for meal_name, item_list in data.items():
                    # The meal for all these items will be the same
                    meal = Meal.objects.get(name=meal_name)
                    for item_dict in item_list:
                        self.create_menu_item(item_dict, meal, dining_hall)
        except Exception as err:
            message = f"Failed to run manage.py scrape: {err}"
            tb = ''.join(traceback.format_exception(None, err, err.__traceback__))
            self.stdout.write(self.style.ERROR(message))
            self.stdout.write(self.style.ERROR(tb))
            admin = NotifierUser.objects.get(phone_number=settings.ADMIN_PHONE_NUMBER)
            send_twilio_message(admin, message)

    def create_menu_item(self, item_dict, meal, dining_hall):
        """Updates or creates MenuItems from the parameters. Also looks up
        nutrition facts if the db_entry doesn't have any
        Args:
            item_dict (dict): A dictionary from scrape() in this form:
                {'name': 'str', 'item_id': 'str'}
            meal (Meal): The name of the meal
            dining_hall (DiningHall): The name of the dining hall
        """
        # Either make a new entry for item or update the old item.is_being_served to True
        db_entry, was_created = MenuItem.objects.update_or_create(
            {'is_being_served': True},
            name=item_dict['name']
        )
        # Also update the meals and locations
        db_entry.meals.add(meal)
        db_entry.locations.add(dining_hall)
        # Log this MenuItem
        MenuItemLog.objects.create(
            date_served=datetime.date.today(),
            menu_item=db_entry,
            meal=meal,
            dining_hall=dining_hall
        )
        # Get the nutrition facts if necessary
        if not db_entry.has_nutrition_facts:
            try:
                facts = self.get_nutrition_facts(item_dict['item_id'])

                if facts:
                    # Set the attributes of the MenuItem based on the mapping between attrs and labels
                    for attr, label in MenuItem.NUTRITION_MAPPING.items():
                        try:
                            setattr(db_entry, attr, Decimal(facts.get(label, '0.0')))
                        except InvalidOperation:
                            # Failed when trying to convert to decimal. Log it and keep going.
                            self.stdout.write(
                                self.style.ERROR('%s had a non-decimal value. Leaving the default value of 0.0...' % label)
                            )
                            continue
                    # Set the ingredients, allergens, and serving size separate
                    db_entry.serving_size = facts.get('Serving Size', 'Unknown')
                    db_entry.allergens = facts.get('Allergens', 'Unknown')
                    db_entry.ingredients = facts.get('Ingredients', 'Unknown')
                else:
                    self.stdout.write(self.style.WARNING('Found no nutrition facts. Leaving them blank...'))

            except IOError:
                # Couldn't connect. Log it and keep going.
                self.stdout.write(
                    self.style.ERROR('Could not connect to dining.ncsu.edu for the nutrition facts. Leaving them blank...')
                )

        db_entry.save()
        if was_created:
            self.stdout.write(self.style.SUCCESS('Created entry for %s' % str(db_entry)))
        else:
            self.stdout.write(self.style.SUCCESS('Updated entry for %s' % str(db_entry)))

    def get_data(self, meal, raw_html):
        """Turns raw HTML into a dictionary with a list of food items
        Format:
        {'Lunch':
            [
                {'name': 'Macaroni', 'item_id': '1_03132017_lunch_starches_macaroni'},
                {'name': 'Sandwich', 'item_id': '1_03132017_lunch_grains_sandwitch'},
                ...
            ]
        }
        """
        # Use beautifulsoup to get the data
        soup = BeautifulSoup(raw_html, "html.parser")
        # All food items are anchors with the 'dining-menu-item-modal' class
        raw_items = soup.find_all(class_='dining-menu-item-modal')
        if not raw_items:
            self.stdout.write(f'Found no items for {meal}')
            return {meal: []}

        self.stdout.write(f'Processing data for {meal}')

        # Create a list of dictionaries where the keys are the name and the item_id
        dishes = []
        for dish in raw_items:
            # Use consistent formatting for the dish's name.
            # Remove html garbage and whitespace, then capitalize each word
            name = html.unescape(dish.text)
            name = name.strip()
            name = name.title()
            dishes.append({'name': name, 'item_id': dish.attrs['rel'][0]})
        # Put those dishes in a dictionary so we can associate them with the meal
        return {meal: dishes}


    def scrape(self, date, location):
        """Makes a GET request to dining.ncsu.edu to download raw HTML
        for a given date at a given location.
        :param date: A Python date object.
        :param location: The name of a dining hall. Options: 'Fountain', 'Clark', 'Case'
        """
        self.stdout.write(f"Scraping {location}'s menu for {date}...")
        # Create a urllib3 instance that uses SSL
        # Construct the URL
        base_url = os.environ.get('SCRAPER_URL', 'https://dining.ncsu.edu/wp-admin/admin-ajax.php?action=ncdining_ajax_menu_results')
        # The date must be in iso format: 2017-03-11
        date_param = f'&date={date.isoformat()}'
        # The locations depend on the WordPress page id.
        if location == 'Fountain':
            location_param = '&pid=45'
        elif location == 'Clark':
            location_param = '&pid=46'
        elif location == 'Case':
            location_param = '&pid=47'
        else:
            raise ValueError("The location must be in this list: %s" % ALL_HALLS)
        # Get the data for every meal by changing the meal_param
        all_meal_data = {}
        for meal in ALL_MEALS:
            full_url = f"{base_url}{date_param}{location_param}&meal={meal}"
            try:
                response = requests.get(full_url)
                # Parse the data into a dictionary and add it to all_meal_data
                all_meal_data.update(self.get_data(meal, response.text))
            except IOError as e:
                self.stdout.write(self.style.ERROR(f"Failed to download {full_url}: {e}"))
                # Couldn't connect. Keep looping in case it was just one meal that was broken
                continue
        return all_meal_data


    def get_nutrition_facts(self, item_id):
        """Make a request for the nutrition facts of a given dish. The url is
        constructed based on the rel attribute of each dish. This method is called
        by the scrape command if it finds a dish without nutrition facts already saved.
        """
        self.stdout.write("Getting nutrition facts for %s" % item_id)
        base_url = "https://dining.ncsu.edu/wp-admin/admin-ajax.php?action=ncdining_ajax_get_item_nutrition&item_id="
        full_url = base_url + item_id
        # Make the request and have bs4 parse it
        response = requests.get(full_url)
        soup = BeautifulSoup(response.text, "html.parser")

        # Start building the data
        nutrition_facts = {}
        # The nutrition facts are in rows with this class
        for row in soup.find_all(class_='menu-nutrition-row'):
            # The label is in the <strong> element with a colon at the end that must be removed
            label = html.unescape(row.find('strong').text).rstrip(':')
            # The value is in a span with the menu-nutrition-row-value class
            value = html.unescape(row.find(class_='menu-nutrition-row-value').text)
            nutrition_facts[label] = value

        # Get the allergens from a div
        allergen_div = soup.find(class_='dining-menu-allergen-contains')
        if allergen_div:
            nutrition_facts['Allergens'] = html.unescape(allergen_div.text)
        # Get the ingredients from another div
        ingredients_div = soup.find(class_='menu-dining-menu-modal-ingredients')
        if ingredients_div:
            nutrition_facts['Ingredients'] = html.unescape(ingredients_div.text).lstrip('Ingredients: ')

        return nutrition_facts
