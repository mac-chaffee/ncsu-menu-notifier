# NCSU Menu Notifier v2

[![build status](https://gitlab.com/mac-chaffee/ncsu-menu-notifier/badges/master/build.svg)](https://gitlab.com/mac-chaffee/ncsu-menu-notifier/commits/master)
[![coverage report](https://gitlab.com/mac-chaffee/ncsu-menu-notifier/badges/master/coverage.svg)](https://gitlab.com/mac-chaffee/ncsu-menu-notifier/commits/master)


A website that will notify you when your favorite foods are being served in
the NC State dining halls. This folder contains the older v2 version of NCSU-MN which was replaced by a v3 version on 2025-02-01.

## How it works
Uses a web scraper to download the menu every day. That data is
then parsed and saved in a database. Every day before every meal,
the site checks which dishes are being served for that meal and sends texts via
Twilio to the users who picked that dish.

## Development Environment Setup

0. Download and install all of the following:
    * Python 3.11.11
    * PyCharm
    * `heroku-cli`
    * Postgres 13
    * Gecko WebDriver
    * `ruby`, `ruby-dev`, and the `sass` gem
0. Create the Postgres user and database

    ```postgresql
    CREATE DATABASE ncsumn;
    CREATE USER db_user WITH PASSWORD 'db_password';
    ALTER USER db_user CREATEDB;
    ```

0. Create a virtual environment and install requirements.txt
   ```bash
   python -m venv venv
   source venv/bin/activate
   pip install -r requirements.txt
   ```
0. Create the `NotifierWebsite/.env` file with the following variables
    ```
    DATABASE_URL=postgres://db_user:db_password@localhost:5432/ncsumn
    DJANGO_DEBUG=1
    TWILIO_TEST_ACCOUNT_SID=<omitted>
    TWILIO_TEST_AUTH_TOKEN=<omitted>
    TWILIO_TEST_PHONE_NUMBER=<omitted>
    ```
0. Run the following setup code:
    ```
    python manage.py migrate
    python manage.py createcachetable
    python manage.py initial_setup
    python manage.py createsuperuser
    ```
0. For selenium tests, make sure the run configurations contain the `--debug-mode` flag
0. Configure heroku-cli to refer to the correct heroku app
0. Set up a file watcher for compiling SCSS files in PyCharm:
    * Arguments: `--no-cache --update sass/$FileName$:stylesheets/$FileNameWithoutExtension$.css`
    * Set the GEM_HOME environment variable to `~/.gems`
    * Or manually: `sass ncsumn/static/custom/sass/all.scss ncsumn/static/custom/stylesheets/all.css`

Instead of installing postgres, you can run it in docker instead:
```bash
docker volume create ncsumn
docker run -e POSTGRES_USER=db_user -e POSTGRES_PASSWORD=db_password \
    -e POSTGRES_DB=ncsumn \
    --cpus=0.5 --memory=2g \
    -d -p 5432:5432 -v ncsumn:/var/lib/postgresql/data postgres:13
```

## Deployment

0. Push to the develop branch (including updates to CHANGELOG.md)
0. If the CI build finished successfully, click the triangle play button to "deploy_staging"
0. If there are any migrations, they will run automatically via the `release` line in the Procfile
0. Make sure the app is working on the staging server
0. Merge develop into master and push
0. If the CI build finished successfully, click the triangle play button to "deploy_production"
0. Tag the release using SemVer and push the tag
   (this will trigger a webhook to inform users of the update)
   ```bash
   git tag -a v2.0.2 -m "Security"
   git push --tags
   ```

### Sending fake texts to the API

To send a fake text to the local server (like to test confirmation/demo/login flow), uncomment the signature checking code in ncsumn/api_views.py, then run:

```
curl -iX POST localhost:8000/sms/ -F 'Body=confirm' -F 'From=+12223334444'
```

## Scale testing

Run the following commands to pull the latest database backup and import just the MenuItems into a local database:

```
heroku pg:backups:download
# Delete old data
psql -h localhost -U db_user -d ncsumn -W -p 5432

pg_restore --no-acl --no-owner -h localhost -U db_user -W -p 5432 -d ncsumn --data-only --table=ncsumn_menuitem latest.dump
rm latest.dump  # Remove after use to protect data
```

# Archival

This version of ncsu-menu-notifier was shut down on 2025-03-01. The dish logs have been archived in this repo in the `ARCHIVED_DATA` folder for future analysis. All other data has been deleted. See the README in that folder for more information.
