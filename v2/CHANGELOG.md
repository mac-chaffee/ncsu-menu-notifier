## **Change Log - NCSU Menu Notifier**

### [3.1.0 | Food Filters] (2025-02-15)

The signup page now allows you to filter the list of dishes by dietary preference and allergens. These are all the same filters that you can select on the dining.ncsu.edu website (NCSU-MN pulls from the same data source). This should make it easier to discover new favorite foods!

Note that currently about 50% of the dishes are still missing dietary/allergen info, and thus won't show up in the list if you use any of the filters. New dietary/allergen info is added every day.

### [3.0.0 | From SMS to RSS] (2025-02-01)

NCSU Menu Notifier is proud to have been running since 2016, but running a website like this one for so long can get expensive. In particular, sending SMS text messages is not only expensive, but new regulations around "Application-to-Person" messaging make it difficult for hobby projects like NCSU-MN to comply. As a result, to save NCSU-MN from shutting down, the website has been completely rearchitected to provide alerts using the [RSS protocol](https://en.wikipedia.org/wiki/RSS) instead of text messaging.

RSS has existed for decades and is normally used to subscribe to blog posts or news articles, but it has [recently increased in popularity](https://pluralistic.net/2024/10/16/keep-it-really-simple-stupid/) as a welcome contrast to algorithmic social media feeds and annoying email/text/push notifications that are designed to serve you more ads and collect your personal information. Importantly, an RSS-based version of NCSU-MN can reduce the monthly costs by almost 90%, making it a great choice for the future.

In 30 days, the text messages from NCSU-MN will stop being sent and only the RSS version will be supported. So please migrate as soon as you can!

#### How do I migrate to RSS notifications?

To migrate, please go to the new homepage of [ncsu-mn.com](https://ncsu-mn.com) and follow the instructions to sign up again.

The downside to RSS is that it's a little harder to set up initially. You need to install a feed reader app such as [NetNewsWire](https://apps.apple.com/us/app/netnewswire-rss-reader/id1480640210) (iOS) or [Newsblur](https://newsblur.com/) (cross-platform), then NCSU-MN will provide you an RSS feed link to import into the app. There are more detailed instructions on the home page. If you have any issues, I'd be happy to help via email (which used to be broken but is fixed now) at [bugs@ncsu-mn.com](mailto:bugs@ncsu-mn.com)!

Please note a few things:

* The new version of NCSU-MN only shows dishes served since 2024-01-01, so if you don't see your favorite dish, it may have been renamed at some point.
* The new version of NCSU-MN only updates the RSS feed once per day instead of once per meal like the text messages.
* Rest assured that all of the old data such as phone numbers stored on NCSU-MN will be deleted 30 days from this announcement. The new version of NCSU-MN does not collect or store any personal data.
* If you need a reminder of the dishes you were subscribed to previously, you can text the word "DEMO" to NCSU-MN to see the list.

---

### [2.2.0 | Magic Links] (2021-04-25)
##### Added
- If you text "LOGIN" to the ncsu-mn phone number, you will receive a "magic link" which you can click to be logged in automatically.
    - The links expire after 1 hour, or upon use (whichever comes first)
    - Only you can generate magic links for yourself, unless someone [steals your SIM card](https://en.wikipedia.org/wiki/SIM_swap_scam) or something. Do NOT share the link with anyone since it grants access to your account.
    - When you click on a "magic link", you will see the date/time you last logged in. If the date looks incorrect, please report the incident to bugs@ncsu-mn.com.


### [2.1.0 | Menu Item Logging] (2021-04-12)
##### Added
- NCSU-MN will now log historical data about when and where each dish is served.
    - When adding new dishes to your list of favorites, you should see "(last served 2021-04-12)" which will help differentiate between dishes when NC State slightly changes their official names.
    - The information will also be used to prune the list of menu items periodically. With over 2000 menu item collected over 5+ years, some of them are likely no longer served!
    - Email bugs@ncsu-mn.com if you have ideas for what to do with this data. Heatmap of which dining hall serves a certain dish most often? Graph of popularity over time? Lots of options.

##### Changed
- Updated all dependencies for stability and security

##### Fixed
- Two bugs appeared during this release which prevented notifications from being sent on April 13th. Those have been fixed now.

---

### [2.0.3 | Security] (2019-11-23)
##### Changed
- Updated all dependencies and upgraded to Python 3.8.0

---

### [2.0.2 | Security] (2019-05-01)
##### Changed
- Updated all dependencies (Python version, Python packages, and the operating system)

---

### [2.0.1 | Stabilization] (2017-11-18)
##### Changed
- Fixed a bug where the calorie form would give certain people a 401 error.
- Fixed duplicate dishes appearing in text messages if the dish was being served for more than
one meal in a day.
- Added better logging and tests so bugs like these are easier to find and prevent in the future.
- If you've opted in to receive ncsu-mn updates, you will no longer be notified about minor patches like this one.

---

### [2.0.0 | Calorie Counter] (2017-10-14)
##### Added
- You can track your calorie intake [here](https://ncsu-mn.com/calories/).

##### Changed
- Implemented caching, which means many pages will load quicker.

---

### [1.4.2 | Opt out of summer notifications] (2017-05-19)
##### Added
- You can now specify whether or not you want notifications over the summer in your preferences.

---

### [1.4.1 | Keep you informed of updates] (2017-04-29)
##### Added
- This neat changelog!
- If there's an update to the site, we'll let you know at the end of one of your normal notifications so it's not too obtrusive.
- If it is still too obtrusive for you, you can disable this in your preferences.

##### Changed
- Made the verification codes all lowercase instead of uppercase for easier typing.
- Updated the info on the about page.

---

### [1.4.0 | Pick which meals you care about] (2017-04-22)
##### Added
- You can now specify which meals you want to be notified about in your preferences!
- If you click on your phone number on the preferences page, it will take you to an
API that you can use to change your preferences. Not terribly useful to you yet, but I think it's cool.

##### Changed
- Made changing preferences (specifically removing dishes) more user-friendly.

---

### [1.3.0 | Support for multiple dining halls] (2017-04-13)
##### Added
- The site now downloads data from Case and Clark in addition to Fountain!
- You can change which dining halls you care about on your preferences page.
- On mobile, you have the option of adding ncsu-mn to your home screen since it
is now a [progressive web app](https://developers.google.com/web/progressive-web-apps/).
- You can text "Demo" to see a preview text as if all your favorite foods were being served
at once.

##### Changed
- All pages now use [gzip compression](http://stackoverflow.com/a/16692852), so they load about 1.5-2x faster now.
- All menu items now use title case and have no leading or trailing whitespace, and
all duplicates have been removed.
- Overhauled the administrative side of the site.

---

### [1.2.0 | Web scraper rewrite] (2017-03-11)
##### Changed
- Re-wrote the part of ncsu-mn that downloads the menu so it works with the redesigned
dining.ncsu.edu.

---

### [1.1.0 | Software best practices] (2017-03-09)
##### Added
- The site now uses HTTPS to keep your data safe.
- Wrote software tests. About 90% of the lines of code have been tested.
- Added a code linter and a code style checker.
- Configured continuous integration to constantly test and lint the code.

##### Changed
- The login process is now completely asynchronous (no page refreshes required).
- When you try to login with invalid credentials, you'll get more descriptive error messages now.

---

### [1.0.0 | Initial release] (2016-10-12)
##### Added
- You can sign up with your phone number to receive notifications when they're serving
a specific dish at Fountain.
- You can log in to add more dishes to your account, or delete old ones.
- The website displays well on mobile and desktop.


[3.1.0 | Food Filters]: https://gitlab.com/mac-chaffee/ncsu-menu-notifier/compare/v3.0.0...v3.1.0
[3.0.0 | From SMS to RSS]: https://gitlab.com/mac-chaffee/ncsu-menu-notifier/compare/v2.2.0...v3.0.0
[2.2.0 | Magic Links]: https://gitlab.com/mac-chaffee/ncsu-menu-notifier/compare/v2.1.0...v2.2.0
[2.1.0 | Menu Item Logging]: https://gitlab.com/mac-chaffee/ncsu-menu-notifier/compare/v2.0.3...v2.1.0
[2.0.3 | Security]: https://gitlab.com/mac-chaffee/ncsu-menu-notifier/compare/v2.0.2...v2.0.3
[2.0.2 | Security]: https://gitlab.com/mac-chaffee/ncsu-menu-notifier/compare/v2.0.1...v2.0.2
[2.0.1 | Stabilization]: https://gitlab.com/mac-chaffee/ncsu-menu-notifier/compare/v2.0.0...v2.0.1
[2.0.0 | Calorie Counter]: https://gitlab.com/mac-chaffee/ncsu-menu-notifier/compare/v1.4.1...v2.0.0
[1.4.2 | Opt out of summer notifications]: https://gitlab.com/mac-chaffee/ncsu-menu-notifier/compare/v1.4.1...v1.4.2
[1.4.1 | Keep you informed of updates]: https://gitlab.com/mac-chaffee/ncsu-menu-notifier/compare/v1.4.0...v1.4.1
[1.4.0 | Pick which meals you care about]: https://gitlab.com/mac-chaffee/ncsu-menu-notifier/compare/v1.3.0...v1.4.0
[1.3.0 | Support for multiple dining halls]: https://gitlab.com/mac-chaffee/ncsu-menu-notifier/compare/v1.2.0...v1.3.0
[1.2.0 | Web scraper rewrite]: https://gitlab.com/mac-chaffee/ncsu-menu-notifier/compare/v1.1.0...v1.2.0
[1.1.0 | Software best practices]: https://gitlab.com/mac-chaffee/ncsu-menu-notifier/compare/v1.0.0...v1.1.0
[1.0.0 | Initial release]: https://gitlab.com/mac-chaffee/ncsu-menu-notifier/commits/v1.0.0
